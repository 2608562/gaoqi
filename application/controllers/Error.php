<?php

class ErrorController extends \Yaf\Controller_Abstract{

    public function indexAction() {
        return false;
    }

    public function errorAction($exception) {
        $this->apiError($exception->getCode(), $exception->getMessage());

        return false;
    }

    protected function apiError($code, $msg) {
        if($code < 0xFFF && \Yaf\Application::app()->environ() !== 'develop') {
            $code = 500;
            $msg  = isset($_GET['enable_debug']) ? $msg : 'Internal Server Error';
        }

        $ret = [
            'status' => false,
            'code'  => $code,
            'msg'   => htmlspecialchars($msg)
        ];

        header('Content-type: application/json; charset=utf-8');
        echo \json_encode($ret);
        return false;
    }
}
