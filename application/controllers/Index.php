<?php

class IndexController extends \Base\ApiController {
    
    public function indexAction() {
        return \Response::data('Hello world!');
    }
    
}
