<?php

class ProtocolController extends \Base\AbstractController {

    protected $allowType = [
        'gaoqi'    => 1,
        'member'   => 1,
        'group'    => 1,
        'sparring' => 1,
        'secret'   => 1,
    ];

    public function showAction() {
        $type = strtolower($this->getRequest()->get('type'));
        $data = null;

        if(isset($this->allowType[$type])) {
            $redis = \Yaf\Registry::get('redis');
            $rKey  = sprintf("protocol.%s", $type);
            $data  = $redis->hGetAll($rKey);

            if(empty($data)) {
                $protocolMapper = \Mapper\ProtocolModel::getInstance();
                $protocolModel  = $protocolMapper->findByType($type);

                if($protocolModel instanceof \ProtocolModel) {
                    $data = [
                        'content' => $protocolModel->getContent(),
                        'title' => $protocolModel->getTitle()
                    ];
                    $redis->hMSet($rKey, $data);
                }
            }

            $this->setViewPath(APPLICATION_PATH.'/application/views/');
            $this->_view->assign('data',$data);
        }
    }
}
