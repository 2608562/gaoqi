<?php

namespace Lib;

final class Consts {

    // Token
    const TOKEN_KEY = 'token.lists.%s';

    // expire
    const EXPIRE_5MINU  = 300;
    const EXPIRE_15MINU = 900;
    const EXPIRE_1HOUR  = 3600;
    const EXPIRE_2HOUR  = 7200;
    const EXPIRE_1DAY   = 86400;
    const EXPIRE_2DAY   = 172800;
    const EXPIRE_7DAY   = 604800;
    const EXPIRE_30DAY  = 2592000;

    const COMMISSION_RATE = 0.1;

    // Queue
    const QUEUE = 'queue.%s.lists';

    // 短信验证码
    const MOBILEPHONE_MESSAGECODE = 'user.resource.v.messagecode.%s';

    const LOGIN_STATUS_NORMAL = 1;

    const SPARRING_BUY_STATUS_NORMAL = 1;
    const SPARRING_BUY_STATUS_DEL    = 2;

    const SPARRING_DETAIL_STATUS_NORMAL = 1;
    const SPARRING_DETAIL_STATUS_PAY    = 2;
    const SPARRING_DETAIL_STATUS_DEL    = 3;

    const SPARRING_SELL_STATUS_NORMAL = 1;
    const SPARRING_SELL_STATUS_PAY    = 2;
    const SPARRING_SELL_STATUS_DEL    = 3;

    // 朋友圈
    const FRIEND_SUPPORT_NORMAL       = 1;
    const FRIEND_SUPPORT_CANCEL       = 0;
    const FRIEND_CIRCLE_STATUS_NORMAL = 1;
    const FRIEND_CIRCLE_STATUS_DEL    = 2;

    // 电竞圈成员类别
    const GAME_CIRCLE_MEMBER_MASTER   = 1;
    const GAME_CIRCLE_MEMBER_MANAGER  = 2;
    const GAME_CIRCLE_MEMBER_ORDINARY = 3;

    // 电竞圈状态
    const GAME_CIRCLE_NORMAL = 1;
    const GAME_CIRCLE_DELETE = 2;

    // 电竞圈记录
    const GAME_CIRCLE_LOG_JOIN   = 1;
    const GAME_CIRCLE_LOG_QUIT   = 2;
    const GAME_CIRCLE_LOG_CREATE = 3;
    const GAME_CIRCLE_LOG_DELETE = 4;

    const LATITUDE_LONGITUDE_1KM        = 8983;
    const LATITUDE_LONGITUDE_20KM       = 179663;
    const LATITUDE_LONGITUDE_CONVERSION = 1000000;
    const EARTH_RADIUS                  = 6367000; //地球的近似半径

    // 电竞圈消息通知类型
    const GAME_INFORM_RECEIVER_BY_AUTO      = 1; //自动接收（默认）
    const GAME_INFORM_RECEIVER_BY_ASSISTANT = 2; //助手接收
    const GAME_INFORM_NOT_RECEIVER          = 3; //不提醒

    // 反馈信息的状态（默认为未处理）
    const CONFIG_FEEDBACK_NOT_DONE = 0;
    const CONFIG_FEEDBACK_DONE     = 1;

    //member 消息接收设置
    const MEMBER_RECEIVER_INFORM     = 1;  //接收消息（默认）
    const MEMBER_NOT_RECEIVER_INFORM = 2;  //不接收

    //member 通知消息类型
    const MEMBER_SHOW_INFORM = 1;  //显示发送人和概要（默认）
    const MEMBER_HIDE_INFORM = 2;  //只显示发信人 隐藏信息概要

    // 评论类型
    const COMMENT_TYPE_FRIEND_CIRCLE = 1;

    // 评论状态
    const COMMENT_STATUS_NORMAL = 1;
    const COMMENT_STATUS_DEL    = 2;

    // 消息推送
    const MESSAGE_PUSH               = 'user.resource.v.messagepush.%s';
    const MESSAGE_PUSH_FRIEND_CIRCLE = 'friend.circle';

    // 举报
    const INFORM_STATUA_NORMAL    = 1;
    const INFORM_TYPE_GAME_CIRCLE = 1;

    // 订单
    const ORDER_TYPE_SPARRING_BUY    = 1;
    const ORDER_TYPE_SPARRING_SELL   = 2;
    const ORDER_STATUS_NOPAY         = 1;
    const ORDER_STATUS_PAY           = 2;
    const ORDER_STATUS_REFUNDING     = 3;
    const ORDER_STATUS_REFUND_REFUSE = 4;
    const ORDER_STATUS_REFUND_FINISH = 5;
    const ORDER_STATUS_FINISH        = 6;
    const ORDER_STATUS_EVALUATE      = 7;
    const ORDER_STATUS_CLOSE         = 8;
    const ORDER_STATUS_DEL           = 9;

    const ORDER_REFUND_STATUS_REPLY   = 1;
    const ORDER_REFUND_STATUS_AGREE   = 2;//弃用
    const ORDER_REFUND_STATUS_SUCCESS = 3;
    const ORDER_REFUND_STATUS_FORCE   = 4;

    //验证
    const AUTHENTICATION = 1;

    const F_CHARGE           = 1;
    const F_COMMISSION       = 2;
    const F_ORDER_INCOME     = 3;
    const F_WITHDRAW_SUCCESS = 4;
    const F_WITHDRAW_FAILED  = 5;
    const F_WITHDRAWING      = 6;
    const F_REFUND_SUCCESS   = 7;

    const W_TYPE_APPLYING = 1;
    const W_TYPE_SUCCESS  = 2;
    const W_TYPE_FAILURE  = 3;

    const TEMP_ID_PAY_BUY     = 'cF6CZHFdZE53ZJvHZiH2CxgnFVMfmOiChZduOK3tngA';
    const TEMP_ID_PAY_SELL    = 'PaqR_jyvQll5PIFetzhx-wO4SYu47tmvn0pOtmZU0Jw';
    const TEMP_ID_FINISH_BUY  = 'G6HkYjLR-ZzMiYbA0fJ7WTY7DxfXZ2AvB5Isa3n9OuM';
    const TEMP_ID_FINISH_SELL = 'Qcc0FI5kZDFLjC0jzla3zoOSd4YbufLfoA4dNBw5HY0';
    const TEMP_ID_REFUND      = 'cvHjDwgMmsCpSXMlJSxmjgluNGuwGfKcTwSyW0cJXgw';
    const TEMP_ID_NO_PAY      = 'kzJoc5FVCERiC2YaS8DaTIKO7OljMgy5pEP9vZbriBA';
}
