<?php

namespace Lib;

use \Mapper\FinanceLogModel as LogMapper;
use \Mapper\MemberAccountModel as AccountMapper;

final class Finance {
    use \Lib\Traits\Instance;

    // 1收入 2支出 3不变
    protected $fundFlow = [
        1 => 1,
        2 => 2,
        3 => 1,
        4 => 3,
        5 => 1,
        6 => 2,
        7 => 1,
    ];

    // 财务日志
    public function financeLog($uid, $type, $sum) {
        if(!isset($this->fundFlow[$type])) {
            return false;
        }

        $flow         = $this->fundFlow[$type];
        $accoutMapper = AccountMapper::getInstance();
        $accoutModel  = $accoutMapper->findByUid($uid);

        if(!$accoutModel instanceof \MemberAccountModel) {
            return false;
        }

        $nowSum = $accoutModel->getNow_sum();

        if($nowSum < 0) {
            return false;
        }

        $logMapper = LogMapper::getInstance();
        $logModel  = new \FinanceLogModel();
        $logModel->setUid($uid);
        $logModel->setType($type);
        $logModel->setNow_sum($nowSum);
        $logModel->setFund_flow($flow);
        $logModel->setAdd_time(time());
        $logModel->setFlow_sum($sum);

        try {
            $lastId = $logMapper->insert($logModel);
        } catch(\Exception $e) {
            $lastId = 0;
        }

        return ($lastId > 0 ? true : false);
    }

    // 余额变化
    public function flow($uid, $type, $sum, $callback = null) {
        $accoutMapper = AccountMapper::getInstance();
        $accoutModel  = $accoutMapper->findByUid($uid);
        $accoutMapper->begin();

        if($accoutModel instanceof \MemberAccountModel) {
            // 更新
            //===============
            if(!isset($this->fundFlow[$type])) {
                return false;
            }

            $flow = $this->fundFlow[$type];

            if(!$accoutModel instanceof \MemberAccountModel) {
                return false;
            }

            switch($flow) {
                case 1:
                    $nowSum = $accoutModel->getNow_sum() + $sum;
                    break;
                case 2:
                    $nowSum = $accoutModel->getNow_sum() - $sum;
                    break;
                case 3:
                    $nowSum = $accoutModel->getNow_sum();
                    break;
                default:
                    return false;
            }

            if($nowSum < 0) {
                return false;
            }
            //===============
            // 1-充值 2-佣金 3-订单收益 4-提现成功 5-提现失败 6提现中 7-退款
            if($type === 1) {
                $accoutModel->setNow_sum($nowSum);
            } elseif($type === 2) {
                $accoutModel->setNow_sum($nowSum);
            } elseif($type === 3) {
                $accoutModel->setNow_sum($nowSum);
            } elseif($type === 4) {
//                $accoutModel->setNow_sum($nowSum);
                $accoutModel->setWithdraw_sum(0);
                $accoutModel->setUsed_sum($accoutModel->getUsed_sum() + $sum);
            } elseif($type === 5) {
                $accoutModel->setNow_sum($nowSum);
                // 假如有两笔提现如果处理: 多个提现金额变量
                $accoutModel->setWithdraw_sum(0);
            } elseif($type === 6) {
                $accoutModel->setNow_sum($nowSum);
                $accoutModel->setWithdraw_sum($sum);
            } elseif($type === 7) {
                $accoutModel->setNow_sum($nowSum);
            } else {
                return false;
            }

            $accoutModel->setUpdate_time(time());

            try {
                $id = $accoutMapper->update($accoutModel, [
                    'uid' => $accoutModel->getUid(),
                ]);
            } catch(\Exception $e) {
                $id = 0;
            }
        } else {
            // 创建
            $accoutModel = new \MemberAccountModel();
            $accoutModel->setUid($uid);
            $accoutModel->setNow_sum($sum);
            $accoutModel->setUsed_sum(0);
            $accoutModel->setWithdraw_sum(0);
            $accoutModel->setAdd_time(time());

            try {
                $id = $accoutMapper->insert($accoutModel);
            } catch(\Exception $e) {
                $id = 0;
            }
        }

        if(!$this->financeLog($uid, $type, $sum)) {
            $accoutMapper->rollback();
            return false;
        }

        if($callback !== null && call_user_func_array($callback, array($this)) === false) {
            $accoutMapper->rollback();
            return false;
        }

        if($id <= 0) {
            $accoutMapper->rollback();
            return false;
        }

        $accoutMapper->commit();
        return true;
    }

    public function commission($uid, $sum) {
        $accoutMapper = AccountMapper::getInstance();
        $accoutModel  = $accoutMapper->findByUid($uid);

        if(!$accoutModel instanceof \MemberAccountModel) {
            return false;
        }

        if(!$accoutModel instanceof \MemberAccountModel) {
            return false;
        }

        $nowSum = $accoutModel->getNow_sum() - $sum;
//            switch($flow) {
//                case 1:
//                    $nowSum = $accoutModel->getNow_sum() + $sum;
//                    break;
//                case 2:

//                    break;
//                case 3:
//                    $nowSum = $accoutModel->getNow_sum();
//                    break;
//                default:
//                    return false;
//            }

        if($nowSum < 0) {
            return false;
        }
        //===============
        // 1-充值 2-佣金 3-订单收益 4-提现成功 5-提现失败 6提现中 7-退款
//            if($type === 1) {
//                $accoutModel->setNow_sum($nowSum);
//            } elseif($type === 2) {
//                $accoutModel->setNow_sum($nowSum);
//            } elseif($type === 3) {
//                $accoutModel->setNow_sum($nowSum);
//            } elseif($type === 4) {
////                $accoutModel->setNow_sum($nowSum);
//                $accoutModel->setWithdraw_sum(0);
//                $accoutModel->setUsed_sum($accoutModel->getUsed_sum() + $sum);
//            } elseif($type === 5) {
//                $accoutModel->setNow_sum($nowSum);
//                // 假如有两笔提现如果处理: 多个提现金额变量
//                $accoutModel->setWithdraw_sum(0);
//            } elseif($type === 6) {
//                $accoutModel->setNow_sum($nowSum);
//                $accoutModel->setWithdraw_sum($sum);
//            } elseif($type === 7) {
//                $accoutModel->setNow_sum($nowSum);
//            } else {
//                return false;
//            }
        $accoutModel->setNow_sum($nowSum);

        $accoutModel->setUpdate_time(time());

        try {
            $id = $accoutMapper->update($accoutModel, [
                'uid' => $accoutModel->getUid(),
            ]);
        } catch(\Exception $e) {
            $id = 0;
        }

        if($id <= 0) {
            return false;
        }

        if(!$this->financeLog($uid, \Lib\Consts::F_COMMISSION, $sum)) {
            return false;
        }

        return true;
    }

}