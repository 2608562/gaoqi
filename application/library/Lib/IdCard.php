<?php

namespace Lib;

final class IdCard {

    /**
     * @des 验证身份证的有效性
     * @var (string) $idCard_base 身份证本体
     * @var (string) $areaNum 省份
     * @var (string) $dateNum 生日
     * @var (string) $verify_code 校验码
     *
     */
    public static function checkIdCard($idCard) {

        // 只能是18位
        if(strlen($idCard) != 18) {
            return false;
        }

        // 取出本体码
        $idCard_base = substr($idCard, 0, 17);

        //省份
        $areaNum = substr($idCard, 0, 2);

        //城市

        //生日
        $dateNum = substr($idCard, 6, 8);

        // 取出校验码
        $verify_code = substr($idCard, 17, 1);

        //验证城市
        $City = array(
            '11', '12', '13', '14', '15', '21', '22',
            '23', '31', '32', '33', '34', '35', '36',
            '37', '41', '42', '43', '44', '45', '46',
            '50', '51', '52', '53', '54', '61', '62',
            '63', '64', '65', '71', '81', '82', '91'
        );

        if(!in_array($areaNum, $City)) {
            return false;
        }

        //验证生日
        if(!strtotime($dateNum)) {  //检查生日日期是否正确
            return false;
        }

        // 加权因子
        $factor = array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);

        // 校验码对应值
        $verify_code_list = array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');

        // 根据前17位计算校验码
        $total = 0;
        for($i = 0; $i < 17; $i++) {
            $total += substr($idCard_base, $i, 1) * $factor[$i];
        }

        // 取模
        $mod = $total % 11;

        // 比较校验码
        if($verify_code == $verify_code_list[$mod]) {
            return true;
        } else {
            return false;
        }

    }
}
