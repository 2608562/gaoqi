<?php

namespace Lib;

/**
 * 一些常用的判断
 */
class Is {

    /**
     * 是否是邮箱地址
     *
     * @param string $email
     * @return boolean
     */
    static public function email($email) {
        return ($email === \filter_var($email, \FILTER_VALIDATE_EMAIL)) ? true : false;
    }

    /**
     * 是否是有效的大陆手机号码
     *
     * @param string|int $num   号码
     * @return boolean
     */
    static public function mobileNum($num) {
        $num = trim($num);

        // 是否是11位
        if (11 !== strlen($num)) {
            return false;
        }

        $pattern = '/^1[3|4|5|7|8|9][0-9]{9}$/';

        return \preg_match($pattern, $num) ? true : false;
    }

    /**
     * 是否是有效的QQ号码
     *
     * @param string|int $num
     * @return boolean
     */
    static public function qqNum($num) {

        $pattern = '/^[1-9][0-9]{4,9}$/';

        return \preg_match($pattern, $num) ? true : false;
    }

    /**
     * url正则验证
     * @param string $url
     * @return boolean
     */
    static public function url($url){
        $match = '/^(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;\+#]*[\w\-\@?^=%&amp;\+#])?/';
        return preg_match($match, $url) ? true : false;
    }

}
