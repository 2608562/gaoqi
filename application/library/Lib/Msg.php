<?php

namespace Lib;

/**
 * 短信内容
 */
final class Msg {

   const MESSAGE_REGISTER      = '【搞起】您正在注册搞起账号，您的验证码为：123456，请在10分钟内输入。';
   const MESSAGE_LOST_PASSWORD = '【搞起】您正在重置密码，验证码为：%s，请在10分钟内输入。如果不是您本人操作，请及时登录查看';

}
