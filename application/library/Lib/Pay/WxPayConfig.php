<?php

namespace Lib\Pay;

final class WxPayConfig {

    static public function appid() {
        return \Yaf\Registry::get('config')->get('resource.wechat.appid');
    }

    static public function appsecret() {
        return \Yaf\Registry::get('config')->get('resource.wechat.appsecret');
    }

    static public function mchid() {
        return \Yaf\Registry::get('config')->get('resource.wechat.mchid');
    }

    static public function key() {
        return \Yaf\Registry::get('config')->get('resource.wechat.key');
    }

    const SSLCERT_PATH = '../cert/apiclient_cert.pem';
    const SSLKEY_PATH  = '../cert/apiclient_key.pem';

    /**
     * TODO：这里设置代理机器，只有需要代理的时候才设置，不需要代理，请设置为0.0.0.0和0
     * 本例程通过curl使用HTTP POST方法，此处可修改代理服务器，
     * 默认CURL_PROXY_HOST=0.0.0.0和CURL_PROXY_PORT=0，此时不开启代理（如有需要才设置）
     */
    const CURL_PROXY_HOST = "0.0.0.0";//"10.152.18.220";
    const CURL_PROXY_PORT = 0;//8080;

    /**
     * TODO：接口调用上报等级，默认紧错误上报（注意：上报超时间为【1s】，上报无论成败【永不抛出异常】，
     * 上报等级，0.关闭上报; 1.仅错误出错上报; 2.全量上报
     */
    const REPORT_LEVENL = 1;
}
