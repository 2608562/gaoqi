<?php

namespace Lib\Pay;

class WxPayException extends \Exception {
    public function errorMessage() {
        return $this->getMessage();
    }
}
