<?php

namespace Lib;

final class Std extends \stdClass {

    public function __construct(array $from = array()) {
        !empty($from) && $this->push($from);
    }

    private function __sleep() {}

    public function __set($name, $value) {
        if(preg_match('/^[a-z][a-z0-9\_]*$/i', $name)) {
            $this->{$name} = $value;
        }
    }

    public function __get($name) {
        return (isset($this->{$name}) ? $this->{$name} : '');
    }

    /**
     *  提供get*方法
     *
     * @param string $name
     * @param string $arguments
     * @return \Ku\Std|null
     */
    public function __call($name, $arguments = null) {
        $ret = null;

        if (strpos($name, 'get') === 0 && empty($arguments)) {
            $name = lcfirst(trim(substr($name, 3)));
            
            $ret = $this->get($name);
        }

        return $ret;
    }
    
    /**
     * 数据压入
     * 
     * @param array $from
     * @return \Lib\Std
     */
    public function push(array $from) {
        foreach ($from as $key => $value) {
            $this->set($key, $value);
        }
        
        return $this;
    }

    /**
     *
     * @param strig $name
     * @param string|array|boolean|number $value
     * @return \Lib\Std
     */
    public function set($name, $value){
        $this->{$name} = $value;

        return $this;
    }

    /**
     *
     * @param strig $name
     * @return string|array|boolean|number $value
     */
    public function get($name){
        return $this->{$name};
    }

    /**
     * 属性转成数组
     *
     * @return \array
     */
    public function toArray(){
        $items = array();

        foreach ($this as $key => $value){
            $items[$key] = $value;
        }

        return $items;
    }
}