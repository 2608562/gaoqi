<?php

namespace Lib;

final class Tool {

    /**
     * @desc 计算年龄
     *
     * @param $birthday
     * @return bool|string
     */
    static public function age($birthday) {
        $birthday = (int)$birthday;
        $age      = date('Y') - date('Y', $birthday) - 1;

        if(date('m', time()) == date('m', $birthday)) {
            if(date('d', time()) >= date('d', $birthday)) {
                ++$age;
            }
        } elseif(date('m', time()) > date('m', $birthday)) {
            ++$age;
        }

        return $age;
    }

    /**
     * @desc 根据两点间的经纬度计算距离
     *
     * @param $lng1 float 经度值
     * @param $lat1 float 纬度值
     * @param $lng2 float
     * @param $lat2 float
     * @return float
     */
    static public function getDistanceAction($lng1, $lat1, $lng2, $lat2) {
        $earthRadius = \Lib\Consts::EARTH_RADIUS;
        $lat1        = ($lat1 * pi()) / 180;
        $lng1        = ($lng1 * pi()) / 180;
        $lat2        = ($lat2 * pi()) / 180;
        $lng2        = ($lng2 * pi()) / 180;

        $calcLongitude      = $lng2 - $lng1;
        $calcLatitude       = $lat2 - $lat1;
        $stepOne            = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);
        $stepTwo            = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;

        return round($calculatedDistance);
    }

}
