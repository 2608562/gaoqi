<?php

namespace Lib;

final class Trace {

    private static $_id   = [];
    private static $_ip   = [];
    private static $_data = [];

    private function __construct(){}
    private function __clone(){}
    private function __sleep(){}

    /**
     * 日志记录下来
     *
     * @param string $file
     * @param string $log
     *
     * @return boolean
     */
    public static function log($file, $log) {
        if(!file_exists($file)) {
            @touch($file);
        }

        if(is_array($log)) {
            $log = implode(' ', $log);
        } else if(is_object($log)) {
            $log = serialize($log);
        }

        \file_put_contents($file, sprintf('[%s] %s', date('Y-m-d H:i:s') , $log . PHP_EOL), \FILE_APPEND);
    }

    /**
     * 信息输出处理
     *
     * 目前是直接输出在 crontab 做重定写入或输出
     *
     * @param string $log
     *
     * @return boolean
     */
    public static function output($log) {
        echo '[' . date('Y-m-d H:i:s') . '] ' . $log . PHP_EOL;
    }

    /**
     * 数据跟踪
     *
     * @param string $key
     * @param int $value
     * @return int
     */
    public static function data($key, $value = 0) {
        if(!isset(self::$_data[$key]) || self::$_data[$key] === 0) {
            self::$_data[$key] = (int)$value;
        }

        return (self::$_data[$key]);
    }

    /**
     * IP 跟踪
     *
     * @param string $ip
     *
     * @return int
     */
    public static function ip($ip){
        if(!isset(self::$_ip[$ip])) {
            self::$_ip[$ip] = 0;
        }

        return (++self::$_ip[$ip]);
    }

    /**
     * ID跟踪
     *
     * @param string|int $id
     *
     * @return int
     */
    public static function id($id){
        if(!isset(self::$_id[$id])) {
            self::$_id[$id] = 0;
        }

        return (++self::$_id[$id]);
    }

    /**
     * ID计数引用
     *
     * @param string|int $id
     *
     * @return int
     */
    public static function idRef($id){
        return (isset(self::$_id[$id]) ? self::$_id[$id] : 0);
    }

    /**
     * 清空所有的跟踪数据
     *
     * @return true
     */
    public static function flush() {
        self::$_id = [];

        return true;
    }
}
