<?php

namespace Lib\Traits;

trait Instance {

    protected static $_instance = null;

    /**
     * 单例
     */
    public static function getInstance(){
        if (!self::$_instance instanceof self){
            self::$_instance = new self();
        }

        return self::$_instance;
    }
}

