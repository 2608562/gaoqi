<?php

namespace Lib\Traits;

trait StaticClass {
    
    private function __construct(){}
    private function __clone(){}
    private function __sleep(){}
    
}