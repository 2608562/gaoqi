<?php

namespace Lib;

class Verify {

    /**
     * 验证是否邮箱地址
     *
     * @param string $email
     * @return boolean
     */
    public static function isEmail($email) {
        return (bool)(preg_match('/^[a-z0-9][a-z0-9\_\.]*[a-z0-9\_]\@[a-z0-9]+\.[a-z][a-z\.]+$/i', $email));
    }

    /**
     * 验证手机号
     *
     * @param string $phone
     * @return boolean
     */
    public static function isMobile($phone) {
        return (bool)(preg_match('/^(\+86)?(1[3|4|5|8][0-9]{9})$/', $phone));
    }
    
    /**
     * 验证是否可以用于用户名
     *
     * @param string $username
     * @return boolean
     */
    public static function isUsername($username) {
        return (bool)(preg_match('/^[a-z][a-z0-9\_\.]*$/i', $username));
    }

    /**
     * 验证QQ
     *
     * @param number $qq
     * @return boolean
     */
    public static function isQQ($qq) {
        return (bool)(preg_match('/^[1-9][0-9]{4,13}$/', $qq));
    }

    /**
     * 验证是否是URL
     *
     * @param string $url
     */
    public static function isUrl($url) {
        $pregURL = '/^(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;\+#]*[\w\-\@?^=%&amp;\+#])?/';

        return (bool)(preg_match($pregURL, $url));
    }

    /**
     * 验证是否是域名
     *
     * @param string $domain
     * @return boolean
     */
    public static function isDomain($domain) {
        return (bool)(preg_match('/([a-z0-9]+\.)*([a-z0-9][a-z0-9\-]*)\.([a-z]{2,9})/i', $domain));
    }
    
    /**
     * 是否中文名
     * 
     * @param string $name
     * @return boolean
     */
    public static function isCnName($name) {
        return (bool)(preg_match('/^[\x{4e00}-\x{9fa5}]+$/u', $name));
    }
    
    /**
     * 是否是中国身份证号码(18位)
     * 
     * (1)十七位数字本体码加权求和公式 
     *      S = Sum(Ai * Wi), i = 0, ... , 16 ，先对前17位数字的权求和 
     *      Ai:表示第i位置上的身份证号码数字值(0~9) 
     *      Wi:7 9 10 5 8 4 2 1 6 3 7 9 10 5 8 4 2 （表示第i位置上的加权因子）
     * (2)计算模 
     *      Y = mod(S, 11)
     * (3)根据模，查找得到对应的校验码 
     *      Y: 0 1 2 3 4 5 6 7 8 9 10
     *      校验码: 1 0 X 9 8 7 6 5 4 3 2
     * 
     * @param number $id
     * @return boolean
     */
    public static function isID($id) {
        // 加权因子
        $wi = array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
        // 校验码
        $vi = array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');
        
        $ni  = 0;
        $ids = (string)$id;
        $len = strlen($ids) - 1;
            
        for($i = 0, $max = $len; $i < $max; $i++) {
            $aiv = (int)($ids[$i] ?: 0);
            $wiv = (int)($wi[$i] ?: 0);
            
            $ni += ($aiv*$wiv);
        }

        return (bool)(strcasecmp((string)($vi[$ni%11] ?: -1), (string)($ids[$len] ?: -2)) === 0);
    }
}
