<?php

/**
 * Admin
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: admin
 */
class AdminModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * Username
     * 
     * Column Type: varchar(30)
     * 
     * @var string
     */
    protected $_username = '';

    /**
     * ''
     * 
     * Column Type: varchar(30)
     * 
     * @var string
     */
    protected $_nickname = '';

    /**
     * Password
     * 
     * Column Type: char(43)
     * 
     * @var string
     */
    protected $_password = '';

    /**
     * Last_login_ip
     * 
     * Column Type: char(15)
     * Default: 0
     * 
     * @var string
     */
    protected $_lastLoginIp = 0;

    /**
     * Last_login_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_lastLoginTime = 0;

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_updateTime = 0;

    /**
     * Status
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_status = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \AdminModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * Username
     * 
     * Column Type: varchar(30)
     * 
     * @param string $username
     * @return \AdminModel
     */
    public function setUsername($username) {
        $this->_username = (string)$username;

        return $this;
    }

    /**
     * Username
     * 
     * Column Type: varchar(30)
     * 
     * @return string
     */
    public function getUsername() {
        return $this->_username;
    }

    /**
     * ''
     * 
     * Column Type: varchar(30)
     * 
     * @param string $nickname
     * @return \AdminModel
     */
    public function setNickname($nickname) {
        $this->_nickname = (string)$nickname;

        return $this;
    }

    /**
     * ''
     * 
     * Column Type: varchar(30)
     * 
     * @return string
     */
    public function getNickname() {
        return $this->_nickname;
    }

    /**
     * Password
     * 
     * Column Type: char(43)
     * 
     * @param string $password
     * @return \AdminModel
     */
    public function setPassword($password) {
        $this->_password = (string)$password;

        return $this;
    }

    /**
     * Password
     * 
     * Column Type: char(43)
     * 
     * @return string
     */
    public function getPassword() {
        return $this->_password;
    }

    /**
     * Last_login_ip
     * 
     * Column Type: char(15)
     * Default: 0
     * 
     * @param string $lastLoginIp
     * @return \AdminModel
     */
    public function setLast_login_ip($lastLoginIp) {
        $this->_lastLoginIp = (string)$lastLoginIp;

        return $this;
    }

    /**
     * Last_login_ip
     * 
     * Column Type: char(15)
     * Default: 0
     * 
     * @return string
     */
    public function getLast_login_ip() {
        return $this->_lastLoginIp;
    }

    /**
     * Last_login_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $lastLoginTime
     * @return \AdminModel
     */
    public function setLast_login_time($lastLoginTime) {
        $this->_lastLoginTime = (int)$lastLoginTime;

        return $this;
    }

    /**
     * Last_login_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getLast_login_time() {
        return $this->_lastLoginTime;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addTime
     * @return \AdminModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $updateTime
     * @return \AdminModel
     */
    public function setUpdate_time($updateTime) {
        $this->_updateTime = (int)$updateTime;

        return $this;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getUpdate_time() {
        return $this->_updateTime;
    }

    /**
     * Status
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $status
     * @return \AdminModel
     */
    public function setStatus($status) {
        $this->_status = (int)$status;

        return $this;
    }

    /**
     * Status
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'              => $this->_id,
            'username'        => $this->_username,
            'nickname'        => $this->_nickname,
            'password'        => $this->_password,
            'last_login_ip'   => $this->_lastLoginIp,
            'last_login_time' => $this->_lastLoginTime,
            'add_time'        => $this->_addTime,
            'update_time'     => $this->_updateTime,
            'status'          => $this->_status
        );
    }

}
