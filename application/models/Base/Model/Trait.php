<?php

namespace Base\Model;

trait TraitModel {

    protected static $_instance = null;

    /**
     * 单例
     *
     * @return \Mapper\Base\AbstractModel
     */
    public static function getInstance() {
        if(!self::$_instance instanceof self) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }
}
