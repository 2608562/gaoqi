<?php

/**
 * Comment
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: comment
 */
class CommentModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * 评论id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_cid = 0;

    /**
     * Content
     * 
     * Column Type: varchar(255)
     * 
     * @var string
     */
    protected $_content = '';

    /**
     * 评论者uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_fromUid = 0;

    /**
     * 被评论者uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_toUid = 0;

    /**
     * 状态 1正常 2删除
     * 
     * Column Type: tinyint(1)
     * Default: 0
     * 
     * @var int
     */
    protected $_status = 0;

    /**
     * 添加时间
     * 
     * Column Type: int(10)
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * 修改时间(删除)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_updateTime = 0;

    /**
     * 1-朋友圈
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_type = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \CommentModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * 评论id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $cid
     * @return \CommentModel
     */
    public function setCid($cid) {
        $this->_cid = (int)$cid;

        return $this;
    }

    /**
     * 评论id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getCid() {
        return $this->_cid;
    }

    /**
     * Content
     * 
     * Column Type: varchar(255)
     * 
     * @param string $content
     * @return \CommentModel
     */
    public function setContent($content) {
        $this->_content = (string)$content;

        return $this;
    }

    /**
     * Content
     * 
     * Column Type: varchar(255)
     * 
     * @return string
     */
    public function getContent() {
        return $this->_content;
    }

    /**
     * 评论者uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $fromUid
     * @return \CommentModel
     */
    public function setFrom_uid($fromUid) {
        $this->_fromUid = (int)$fromUid;

        return $this;
    }

    /**
     * 评论者uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getFrom_uid() {
        return $this->_fromUid;
    }

    /**
     * 被评论者uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $toUid
     * @return \CommentModel
     */
    public function setTo_uid($toUid) {
        $this->_toUid = (int)$toUid;

        return $this;
    }

    /**
     * 被评论者uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getTo_uid() {
        return $this->_toUid;
    }

    /**
     * 状态 1正常 2删除
     * 
     * Column Type: tinyint(1)
     * Default: 0
     * 
     * @param int $status
     * @return \CommentModel
     */
    public function setStatus($status) {
        $this->_status = (int)$status;

        return $this;
    }

    /**
     * 状态 1正常 2删除
     * 
     * Column Type: tinyint(1)
     * Default: 0
     * 
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * 添加时间
     * 
     * Column Type: int(10)
     * Default: 0
     * 
     * @param int $addTime
     * @return \CommentModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * 添加时间
     * 
     * Column Type: int(10)
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * 修改时间(删除)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $updateTime
     * @return \CommentModel
     */
    public function setUpdate_time($updateTime) {
        $this->_updateTime = (int)$updateTime;

        return $this;
    }

    /**
     * 修改时间(删除)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getUpdate_time() {
        return $this->_updateTime;
    }

    /**
     * 1-朋友圈
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $type
     * @return \CommentModel
     */
    public function setType($type) {
        $this->_type = (int)$type;

        return $this;
    }

    /**
     * 1-朋友圈
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getType() {
        return $this->_type;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'          => $this->_id,
            'cid'         => $this->_cid,
            'content'     => $this->_content,
            'from_uid'    => $this->_fromUid,
            'to_uid'      => $this->_toUid,
            'status'      => $this->_status,
            'add_time'    => $this->_addTime,
            'update_time' => $this->_updateTime,
            'type'        => $this->_type
        );
    }

}
