<?php

/**
 * Complaint
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: complaint
 */
class ComplaintModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * Content
     * 
     * Column Type: varchar(255)
     * 
     * @var string
     */
    protected $_content = '';

    /**
     * 图片链接-3张
     * 
     * Column Type: varchar(255)
     * 
     * @var string
     */
    protected $_imgUrl = '';

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_updateTime = 0;

    /**
     * 1-待处理 2-已处理
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_status = 0;

    /**
     * Order_id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_orderId = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \ComplaintModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * Content
     * 
     * Column Type: varchar(255)
     * 
     * @param string $content
     * @return \ComplaintModel
     */
    public function setContent($content) {
        $this->_content = (string)$content;

        return $this;
    }

    /**
     * Content
     * 
     * Column Type: varchar(255)
     * 
     * @return string
     */
    public function getContent() {
        return $this->_content;
    }

    /**
     * 图片链接-3张
     * 
     * Column Type: varchar(255)
     * 
     * @param string $imgUrl
     * @return \ComplaintModel
     */
    public function setImg_url($imgUrl) {
        $this->_imgUrl = (string)$imgUrl;

        return $this;
    }

    /**
     * 图片链接-3张
     * 
     * Column Type: varchar(255)
     * 
     * @return string
     */
    public function getImg_url() {
        return $this->_imgUrl;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addTime
     * @return \ComplaintModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $updateTime
     * @return \ComplaintModel
     */
    public function setUpdate_time($updateTime) {
        $this->_updateTime = (int)$updateTime;

        return $this;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getUpdate_time() {
        return $this->_updateTime;
    }

    /**
     * 1-待处理 2-已处理
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $status
     * @return \ComplaintModel
     */
    public function setStatus($status) {
        $this->_status = (int)$status;

        return $this;
    }

    /**
     * 1-待处理 2-已处理
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * Order_id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $orderId
     * @return \ComplaintModel
     */
    public function setOrder_id($orderId) {
        $this->_orderId = (int)$orderId;

        return $this;
    }

    /**
     * Order_id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getOrder_id() {
        return $this->_orderId;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'          => $this->_id,
            'content'     => $this->_content,
            'img_url'     => $this->_imgUrl,
            'add_time'    => $this->_addTime,
            'update_time' => $this->_updateTime,
            'status'      => $this->_status,
            'order_id'    => $this->_orderId
        );
    }

}
