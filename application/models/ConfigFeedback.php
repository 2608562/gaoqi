<?php

/**
 * 意见反馈
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: config_feedback
 */
class ConfigFeedbackModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * 用户的ID
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_memberId = 0;

    /**
     * 投诉的类型，1-咨询，2-投诉，3-建议
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_type = 0;

    /**
     * 评论的内容
     * 
     * Column Type: varchar(255)
     * 
     * @var string
     */
    protected $_content = '';

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * 状态，0-待处理，1-已处理
     * 
     * Column Type: tinyint(1) unsigned zerofill
     * Default: 0
     * 
     * @var int
     */
    protected $_status = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \ConfigFeedbackModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * 用户的ID
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $memberId
     * @return \ConfigFeedbackModel
     */
    public function setMember_id($memberId) {
        $this->_memberId = (int)$memberId;

        return $this;
    }

    /**
     * 用户的ID
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getMember_id() {
        return $this->_memberId;
    }

    /**
     * 投诉的类型，1-咨询，2-投诉，3-建议
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $type
     * @return \ConfigFeedbackModel
     */
    public function setType($type) {
        $this->_type = (int)$type;

        return $this;
    }

    /**
     * 投诉的类型，1-咨询，2-投诉，3-建议
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getType() {
        return $this->_type;
    }

    /**
     * 评论的内容
     * 
     * Column Type: varchar(255)
     * 
     * @param string $content
     * @return \ConfigFeedbackModel
     */
    public function setContent($content) {
        $this->_content = (string)$content;

        return $this;
    }

    /**
     * 评论的内容
     * 
     * Column Type: varchar(255)
     * 
     * @return string
     */
    public function getContent() {
        return $this->_content;
    }

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addTime
     * @return \ConfigFeedbackModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * 状态，0-待处理，1-已处理
     * 
     * Column Type: tinyint(1) unsigned zerofill
     * Default: 0
     * 
     * @param int $status
     * @return \ConfigFeedbackModel
     */
    public function setStatus($status) {
        $this->_status = (int)$status;

        return $this;
    }

    /**
     * 状态，0-待处理，1-已处理
     * 
     * Column Type: tinyint(1) unsigned zerofill
     * Default: 0
     * 
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'        => $this->_id,
            'member_id' => $this->_memberId,
            'type'      => $this->_type,
            'content'   => $this->_content,
            'add_time'  => $this->_addTime,
            'status'    => $this->_status
        );
    }

}
