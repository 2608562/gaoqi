<?php

/**
 * Evaluate
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: evaluate
 */
class EvaluateModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * 星星数
     * 
     * Column Type: tinyint(1)
     * Default: 0
     * 
     * @var int
     */
    protected $_starNum = 0;

    /**
     * 订单id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_orderId = 0;

    /**
     * Content
     * 
     * Column Type: varchar(255)
     * 
     * @var string
     */
    protected $_content = null;

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \EvaluateModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * 星星数
     * 
     * Column Type: tinyint(1)
     * Default: 0
     * 
     * @param int $starNum
     * @return \EvaluateModel
     */
    public function setStar_num($starNum) {
        $this->_starNum = (int)$starNum;

        return $this;
    }

    /**
     * 星星数
     * 
     * Column Type: tinyint(1)
     * Default: 0
     * 
     * @return int
     */
    public function getStar_num() {
        return $this->_starNum;
    }

    /**
     * 订单id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $orderId
     * @return \EvaluateModel
     */
    public function setOrder_id($orderId) {
        $this->_orderId = (int)$orderId;

        return $this;
    }

    /**
     * 订单id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getOrder_id() {
        return $this->_orderId;
    }

    /**
     * Content
     * 
     * Column Type: varchar(255)
     * 
     * @param string $content
     * @return \EvaluateModel
     */
    public function setContent($content) {
        $this->_content = (string)$content;

        return $this;
    }

    /**
     * Content
     * 
     * Column Type: varchar(255)
     * 
     * @return string
     */
    public function getContent() {
        return $this->_content;
    }

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addTime
     * @return \EvaluateModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'       => $this->_id,
            'star_num' => $this->_starNum,
            'order_id' => $this->_orderId,
            'content'  => $this->_content,
            'add_time' => $this->_addTime
        );
    }

}
