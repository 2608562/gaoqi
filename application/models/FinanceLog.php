<?php

/**
 * Finance_log
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: finance_log
 */
class FinanceLogModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * Uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_uid = 0;

    /**
     * 现金余额
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @var float
     */
    protected $_nowSum = 0.00;

    /**
     * 变动金额
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @var float
     */
    protected $_flowSum = 0.00;

    /**
     * 变化: 1-收入 2-支出
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_fundFlow = 0;

    /**
     * 1-充值 2-佣金 3-订单收益 4-提现 5-提现失败
     * 
     * Column Type: tinyint(3) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_type = 0;

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \FinanceLogModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * Uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $uid
     * @return \FinanceLogModel
     */
    public function setUid($uid) {
        $this->_uid = (int)$uid;

        return $this;
    }

    /**
     * Uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getUid() {
        return $this->_uid;
    }

    /**
     * 现金余额
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @param float $nowSum
     * @return \FinanceLogModel
     */
    public function setNow_sum($nowSum) {
        $this->_nowSum = (float)$nowSum;

        return $this;
    }

    /**
     * 现金余额
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @return float
     */
    public function getNow_sum() {
        return $this->_nowSum;
    }

    /**
     * 变动金额
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @param float $flowSum
     * @return \FinanceLogModel
     */
    public function setFlow_sum($flowSum) {
        $this->_flowSum = (float)$flowSum;

        return $this;
    }

    /**
     * 变动金额
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @return float
     */
    public function getFlow_sum() {
        return $this->_flowSum;
    }

    /**
     * 变化: 1-收入 2-支出
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $fundFlow
     * @return \FinanceLogModel
     */
    public function setFund_flow($fundFlow) {
        $this->_fundFlow = (int)$fundFlow;

        return $this;
    }

    /**
     * 变化: 1-收入 2-支出
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getFund_flow() {
        return $this->_fundFlow;
    }

    /**
     * 1-充值 2-佣金 3-订单收益 4-提现 5-提现失败
     * 
     * Column Type: tinyint(3) unsigned
     * Default: 0
     * 
     * @param int $type
     * @return \FinanceLogModel
     */
    public function setType($type) {
        $this->_type = (int)$type;

        return $this;
    }

    /**
     * 1-充值 2-佣金 3-订单收益 4-提现 5-提现失败
     * 
     * Column Type: tinyint(3) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getType() {
        return $this->_type;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addTime
     * @return \FinanceLogModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'        => $this->_id,
            'uid'       => $this->_uid,
            'now_sum'   => $this->_nowSum,
            'flow_sum'  => $this->_flowSum,
            'fund_flow' => $this->_fundFlow,
            'type'      => $this->_type,
            'add_time'  => $this->_addTime
        );
    }

}
