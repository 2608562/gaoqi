<?php

/**
 * Friend
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: friend
 */
class FriendModel extends \Base\Model\AbstractModel {

    /**
     * id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * 用户id
     * 
     * Column Type: int(10) unsigned
     * 
     * @var int
     */
    protected $_memberId = null;

    /**
     * 好友id
     * 
     * Column Type: int(10) unsigned
     * 
     * @var int
     */
    protected $_friendId = null;

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * 
     * @var int
     */
    protected $_addTime = null;

    /**
     * id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \FriendModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * 用户id
     * 
     * Column Type: int(10) unsigned
     * 
     * @param int $memberId
     * @return \FriendModel
     */
    public function setMember_id($memberId) {
        $this->_memberId = (int)$memberId;

        return $this;
    }

    /**
     * 用户id
     * 
     * Column Type: int(10) unsigned
     * 
     * @return int
     */
    public function getMember_id() {
        return $this->_memberId;
    }

    /**
     * 好友id
     * 
     * Column Type: int(10) unsigned
     * 
     * @param int $friendId
     * @return \FriendModel
     */
    public function setFriend_id($friendId) {
        $this->_friendId = (int)$friendId;

        return $this;
    }

    /**
     * 好友id
     * 
     * Column Type: int(10) unsigned
     * 
     * @return int
     */
    public function getFriend_id() {
        return $this->_friendId;
    }

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * 
     * @param int $addTime
     * @return \FriendModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'        => $this->_id,
            'member_id' => $this->_memberId,
            'friend_id' => $this->_friendId,
            'add_time'  => $this->_addTime
        );
    }

}
