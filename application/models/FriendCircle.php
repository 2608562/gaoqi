<?php

/**
 * Friend_circle
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: friend_circle
 */
class FriendCircleModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * Member_id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_memberId = 0;

    /**
     * Content
     * 
     * Column Type: varchar(255)
     * 
     * @var string
     */
    protected $_content = '';

    /**
     * Img_url
     * 
     * Column Type: varchar(255)
     * 
     * @var string
     */
    protected $_imgUrl = '';

    /**
     * 经度(最大180)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_longitude = 0;

    /**
     * 纬度(最大90)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_latitude = 0;

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * 1-正常 2-删除
     * 
     * Column Type: tinyint(1)
     * Default: 0
     * 
     * @var int
     */
    protected $_status = 0;

    /**
     * 性别
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_gender = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \FriendCircleModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * Member_id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $memberId
     * @return \FriendCircleModel
     */
    public function setMember_id($memberId) {
        $this->_memberId = (int)$memberId;

        return $this;
    }

    /**
     * Member_id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getMember_id() {
        return $this->_memberId;
    }

    /**
     * Content
     * 
     * Column Type: varchar(255)
     * 
     * @param string $content
     * @return \FriendCircleModel
     */
    public function setContent($content) {
        $this->_content = (string)$content;

        return $this;
    }

    /**
     * Content
     * 
     * Column Type: varchar(255)
     * 
     * @return string
     */
    public function getContent() {
        return $this->_content;
    }

    /**
     * Img_url
     * 
     * Column Type: varchar(255)
     * 
     * @param string $imgUrl
     * @return \FriendCircleModel
     */
    public function setImg_url($imgUrl) {
        $this->_imgUrl = (string)$imgUrl;

        return $this;
    }

    /**
     * Img_url
     * 
     * Column Type: varchar(255)
     * 
     * @return string
     */
    public function getImg_url() {
        return $this->_imgUrl;
    }

    /**
     * 经度(最大180)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $longitude
     * @return \FriendCircleModel
     */
    public function setLongitude($longitude) {
        $this->_longitude = (int)$longitude;

        return $this;
    }

    /**
     * 经度(最大180)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getLongitude() {
        return $this->_longitude;
    }

    /**
     * 纬度(最大90)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $latitude
     * @return \FriendCircleModel
     */
    public function setLatitude($latitude) {
        $this->_latitude = (int)$latitude;

        return $this;
    }

    /**
     * 纬度(最大90)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getLatitude() {
        return $this->_latitude;
    }

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addTime
     * @return \FriendCircleModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * 1-正常 2-删除
     * 
     * Column Type: tinyint(1)
     * Default: 0
     * 
     * @param int $status
     * @return \FriendCircleModel
     */
    public function setStatus($status) {
        $this->_status = (int)$status;

        return $this;
    }

    /**
     * 1-正常 2-删除
     * 
     * Column Type: tinyint(1)
     * Default: 0
     * 
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * 性别
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $gender
     * @return \FriendCircleModel
     */
    public function setGender($gender) {
        $this->_gender = (int)$gender;

        return $this;
    }

    /**
     * 性别
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getGender() {
        return $this->_gender;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'        => $this->_id,
            'member_id' => $this->_memberId,
            'content'   => $this->_content,
            'img_url'   => $this->_imgUrl,
            'longitude' => $this->_longitude,
            'latitude'  => $this->_latitude,
            'add_time'  => $this->_addTime,
            'status'    => $this->_status,
            'gender'    => $this->_gender
        );
    }

}
