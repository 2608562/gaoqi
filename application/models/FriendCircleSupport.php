<?php

/**
 * Friend_circle_support
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: friend_circle_support
 */
class FriendCircleSupportModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * 动态信息id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_friendCircleId = 0;

    /**
     * 点赞用户id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_friendId = 0;

    /**
     * 是否点赞  1是 0否
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_status = 0;

    /**
     * 更新时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_updateTime = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \FriendCircleSupportModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * 动态信息id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $friendCircleId
     * @return \FriendCircleSupportModel
     */
    public function setFriend_circle_id($friendCircleId) {
        $this->_friendCircleId = (int)$friendCircleId;

        return $this;
    }

    /**
     * 动态信息id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getFriend_circle_id() {
        return $this->_friendCircleId;
    }

    /**
     * 点赞用户id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $friendId
     * @return \FriendCircleSupportModel
     */
    public function setFriend_id($friendId) {
        $this->_friendId = (int)$friendId;

        return $this;
    }

    /**
     * 点赞用户id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getFriend_id() {
        return $this->_friendId;
    }

    /**
     * 是否点赞  1是 0否
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $status
     * @return \FriendCircleSupportModel
     */
    public function setStatus($status) {
        $this->_status = (int)$status;

        return $this;
    }

    /**
     * 是否点赞  1是 0否
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * 更新时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $updateTime
     * @return \FriendCircleSupportModel
     */
    public function setUpdate_time($updateTime) {
        $this->_updateTime = (int)$updateTime;

        return $this;
    }

    /**
     * 更新时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getUpdate_time() {
        return $this->_updateTime;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'               => $this->_id,
            'friend_circle_id' => $this->_friendCircleId,
            'friend_id'        => $this->_friendId,
            'status'           => $this->_status,
            'update_time'      => $this->_updateTime
        );
    }

}
