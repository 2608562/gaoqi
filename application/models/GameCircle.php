<?php

/**
 * Game_circle
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: game_circle
 */
class GameCircleModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * Member_id
     * 
     * Column Type: int(10) unsigned
     * 
     * @var int
     */
    protected $_memberId = null;

    /**
     * 电竞圈名称
     * 
     * Column Type: varchar(30)
     * 
     * @var string
     */
    protected $_name = '';

    /**
     * 地址
     * 
     * Column Type: varchar(50)
     * 
     * @var string
     */
    protected $_address = '';

    /**
     * 环信的群id
     * 
     * Column Type: varchar(50)
     * Default: 0
     * 
     * @var string
     */
    protected $_circleNum = 0;

    /**
     * 主打游戏
     * 
     * Column Type: varchar(30)
     * 
     * @var string
     */
    protected $_mainGame = '';

    /**
     * 总人数
     * 
     * Column Type: smallint(5) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_totalNumber = 0;

    /**
     * 人数
     * 
     * Column Type: smallint(5) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_number = 0;

    /**
     * 圈位置-经度
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_longitude = 0;

    /**
     * 圈位置-纬度
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_latitude = 0;

    /**
     * 电竞圈等级
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_level = 0;

    /**
     * 电竞圈宣言
     * 
     * Column Type: varchar(100)
     * 
     * @var string
     */
    protected $_manifesto = '';

    /**
     * Img_url
     * 
     * Column Type: varchar(255)
     * 
     * @var string
     */
    protected $_imgUrl = '';

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_updateTime = 0;

    /**
     * 状态 1正常 2删除
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_status = 0;

    /**
     * 首字母
     * 
     * Column Type: char(1)
     * 
     * @var string
     */
    protected $_letter = '';

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \GameCircleModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * Member_id
     * 
     * Column Type: int(10) unsigned
     * 
     * @param int $memberId
     * @return \GameCircleModel
     */
    public function setMember_id($memberId) {
        $this->_memberId = (int)$memberId;

        return $this;
    }

    /**
     * Member_id
     * 
     * Column Type: int(10) unsigned
     * 
     * @return int
     */
    public function getMember_id() {
        return $this->_memberId;
    }

    /**
     * 电竞圈名称
     * 
     * Column Type: varchar(30)
     * 
     * @param string $name
     * @return \GameCircleModel
     */
    public function setName($name) {
        $this->_name = (string)$name;

        return $this;
    }

    /**
     * 电竞圈名称
     * 
     * Column Type: varchar(30)
     * 
     * @return string
     */
    public function getName() {
        return $this->_name;
    }

    /**
     * 地址
     * 
     * Column Type: varchar(50)
     * 
     * @param string $address
     * @return \GameCircleModel
     */
    public function setAddress($address) {
        $this->_address = (string)$address;

        return $this;
    }

    /**
     * 地址
     * 
     * Column Type: varchar(50)
     * 
     * @return string
     */
    public function getAddress() {
        return $this->_address;
    }

    /**
     * 环信的群id
     * 
     * Column Type: varchar(50)
     * Default: 0
     * 
     * @param string $circleNum
     * @return \GameCircleModel
     */
    public function setCircle_num($circleNum) {
        $this->_circleNum = (string)$circleNum;

        return $this;
    }

    /**
     * 环信的群id
     * 
     * Column Type: varchar(50)
     * Default: 0
     * 
     * @return string
     */
    public function getCircle_num() {
        return $this->_circleNum;
    }

    /**
     * 主打游戏
     * 
     * Column Type: varchar(30)
     * 
     * @param string $mainGame
     * @return \GameCircleModel
     */
    public function setMain_game($mainGame) {
        $this->_mainGame = (string)$mainGame;

        return $this;
    }

    /**
     * 主打游戏
     * 
     * Column Type: varchar(30)
     * 
     * @return string
     */
    public function getMain_game() {
        return $this->_mainGame;
    }

    /**
     * 总人数
     * 
     * Column Type: smallint(5) unsigned
     * Default: 0
     * 
     * @param int $totalNumber
     * @return \GameCircleModel
     */
    public function setTotal_number($totalNumber) {
        $this->_totalNumber = (int)$totalNumber;

        return $this;
    }

    /**
     * 总人数
     * 
     * Column Type: smallint(5) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getTotal_number() {
        return $this->_totalNumber;
    }

    /**
     * 人数
     * 
     * Column Type: smallint(5) unsigned
     * Default: 0
     * 
     * @param int $number
     * @return \GameCircleModel
     */
    public function setNumber($number) {
        $this->_number = (int)$number;

        return $this;
    }

    /**
     * 人数
     * 
     * Column Type: smallint(5) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getNumber() {
        return $this->_number;
    }

    /**
     * 圈位置-经度
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $longitude
     * @return \GameCircleModel
     */
    public function setLongitude($longitude) {
        $this->_longitude = (int)$longitude;

        return $this;
    }

    /**
     * 圈位置-经度
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getLongitude() {
        return $this->_longitude;
    }

    /**
     * 圈位置-纬度
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $latitude
     * @return \GameCircleModel
     */
    public function setLatitude($latitude) {
        $this->_latitude = (int)$latitude;

        return $this;
    }

    /**
     * 圈位置-纬度
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getLatitude() {
        return $this->_latitude;
    }

    /**
     * 电竞圈等级
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $level
     * @return \GameCircleModel
     */
    public function setLevel($level) {
        $this->_level = (int)$level;

        return $this;
    }

    /**
     * 电竞圈等级
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getLevel() {
        return $this->_level;
    }

    /**
     * 电竞圈宣言
     * 
     * Column Type: varchar(100)
     * 
     * @param string $manifesto
     * @return \GameCircleModel
     */
    public function setManifesto($manifesto) {
        $this->_manifesto = (string)$manifesto;

        return $this;
    }

    /**
     * 电竞圈宣言
     * 
     * Column Type: varchar(100)
     * 
     * @return string
     */
    public function getManifesto() {
        return $this->_manifesto;
    }

    /**
     * Img_url
     * 
     * Column Type: varchar(255)
     * 
     * @param string $imgUrl
     * @return \GameCircleModel
     */
    public function setImg_url($imgUrl) {
        $this->_imgUrl = (string)$imgUrl;

        return $this;
    }

    /**
     * Img_url
     * 
     * Column Type: varchar(255)
     * 
     * @return string
     */
    public function getImg_url() {
        return $this->_imgUrl;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addTime
     * @return \GameCircleModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $updateTime
     * @return \GameCircleModel
     */
    public function setUpdate_time($updateTime) {
        $this->_updateTime = (int)$updateTime;

        return $this;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getUpdate_time() {
        return $this->_updateTime;
    }

    /**
     * 状态 1正常 2删除
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $status
     * @return \GameCircleModel
     */
    public function setStatus($status) {
        $this->_status = (int)$status;

        return $this;
    }

    /**
     * 状态 1正常 2删除
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * 首字母
     * 
     * Column Type: char(1)
     * 
     * @param string $letter
     * @return \GameCircleModel
     */
    public function setLetter($letter) {
        $this->_letter = (string)$letter;

        return $this;
    }

    /**
     * 首字母
     * 
     * Column Type: char(1)
     * 
     * @return string
     */
    public function getLetter() {
        return $this->_letter;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'           => $this->_id,
            'member_id'    => $this->_memberId,
            'name'         => $this->_name,
            'address'      => $this->_address,
            'circle_num'   => $this->_circleNum,
            'main_game'    => $this->_mainGame,
            'total_number' => $this->_totalNumber,
            'number'       => $this->_number,
            'longitude'    => $this->_longitude,
            'latitude'     => $this->_latitude,
            'level'        => $this->_level,
            'manifesto'    => $this->_manifesto,
            'img_url'      => $this->_imgUrl,
            'add_time'     => $this->_addTime,
            'update_time'  => $this->_updateTime,
            'status'       => $this->_status,
            'letter'       => $this->_letter
        );
    }

}
