<?php

/**
 * Game_circle_log
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: game_circle_log
 */
class GameCircleLogModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * Member_id
     * 
     * Column Type: int(10) unsigned
     * 
     * @var int
     */
    protected $_memberId = null;

    /**
     * 1-加入电竞圈  2-退出电竞圈 3-创建电竞圈 4-删除电竞圈
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_status = 0;

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \GameCircleLogModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * Member_id
     * 
     * Column Type: int(10) unsigned
     * 
     * @param int $memberId
     * @return \GameCircleLogModel
     */
    public function setMember_id($memberId) {
        $this->_memberId = (int)$memberId;

        return $this;
    }

    /**
     * Member_id
     * 
     * Column Type: int(10) unsigned
     * 
     * @return int
     */
    public function getMember_id() {
        return $this->_memberId;
    }

    /**
     * 1-加入电竞圈  2-退出电竞圈 3-创建电竞圈 4-删除电竞圈
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $status
     * @return \GameCircleLogModel
     */
    public function setStatus($status) {
        $this->_status = (int)$status;

        return $this;
    }

    /**
     * 1-加入电竞圈  2-退出电竞圈 3-创建电竞圈 4-删除电竞圈
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addTime
     * @return \GameCircleLogModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'        => $this->_id,
            'member_id' => $this->_memberId,
            'status'    => $this->_status,
            'add_time'  => $this->_addTime
        );
    }

}
