<?php

/**
 * Game_circle_member
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: game_circle_member
 */
class GameCircleMemberModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * 用户id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_memberId = 0;

    /**
     * 电竞圈id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_gameCircleId = 0;

    /**
     * 成员类型 1-群主 2管理员 3普通成员
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_type = 0;

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_updateTime = 0;

    /**
     * 1-接收消息并提醒 2-收进圈助手且不提醒 3-屏蔽群消息
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_msgSetting = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \GameCircleMemberModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * 用户id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $memberId
     * @return \GameCircleMemberModel
     */
    public function setMember_id($memberId) {
        $this->_memberId = (int)$memberId;

        return $this;
    }

    /**
     * 用户id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getMember_id() {
        return $this->_memberId;
    }

    /**
     * 电竞圈id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $gameCircleId
     * @return \GameCircleMemberModel
     */
    public function setGame_circle_id($gameCircleId) {
        $this->_gameCircleId = (int)$gameCircleId;

        return $this;
    }

    /**
     * 电竞圈id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getGame_circle_id() {
        return $this->_gameCircleId;
    }

    /**
     * 成员类型 1-群主 2管理员 3普通成员
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $type
     * @return \GameCircleMemberModel
     */
    public function setType($type) {
        $this->_type = (int)$type;

        return $this;
    }

    /**
     * 成员类型 1-群主 2管理员 3普通成员
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getType() {
        return $this->_type;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addTime
     * @return \GameCircleMemberModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $updateTime
     * @return \GameCircleMemberModel
     */
    public function setUpdate_time($updateTime) {
        $this->_updateTime = (int)$updateTime;

        return $this;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getUpdate_time() {
        return $this->_updateTime;
    }

    /**
     * 1-接收消息并提醒 2-收进圈助手且不提醒 3-屏蔽群消息
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $msgSetting
     * @return \GameCircleMemberModel
     */
    public function setMsg_setting($msgSetting) {
        $this->_msgSetting = (int)$msgSetting;

        return $this;
    }

    /**
     * 1-接收消息并提醒 2-收进圈助手且不提醒 3-屏蔽群消息
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getMsg_setting() {
        return $this->_msgSetting;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'             => $this->_id,
            'member_id'      => $this->_memberId,
            'game_circle_id' => $this->_gameCircleId,
            'type'           => $this->_type,
            'add_time'       => $this->_addTime,
            'update_time'    => $this->_updateTime,
            'msg_setting'    => $this->_msgSetting
        );
    }

}
