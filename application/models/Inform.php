<?php

/**
 * 举报
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: inform
 */
class InformModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * Mobile
     * 
     * Column Type: varchar(20)
     * 
     * @var string
     */
    protected $_mobile = '';

    /**
     * 举报内容
     * 
     * Column Type: varchar(255)
     * 
     * @var string
     */
    protected $_content = '';

    /**
     * 举报人id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_uid = 0;

    /**
     * 对应的id (电竞圈)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_cid = 0;

    /**
     * 举报类型 1-电竞圈举报
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_type = 0;

    /**
     * 举报信息状态 1正常 2已处理
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_status = 0;

    /**
     * 举报时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \InformModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * Mobile
     * 
     * Column Type: varchar(20)
     * 
     * @param string $mobile
     * @return \InformModel
     */
    public function setMobile($mobile) {
        $this->_mobile = (string)$mobile;

        return $this;
    }

    /**
     * Mobile
     * 
     * Column Type: varchar(20)
     * 
     * @return string
     */
    public function getMobile() {
        return $this->_mobile;
    }

    /**
     * 举报内容
     * 
     * Column Type: varchar(255)
     * 
     * @param string $content
     * @return \InformModel
     */
    public function setContent($content) {
        $this->_content = (string)$content;

        return $this;
    }

    /**
     * 举报内容
     * 
     * Column Type: varchar(255)
     * 
     * @return string
     */
    public function getContent() {
        return $this->_content;
    }

    /**
     * 举报人id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $uid
     * @return \InformModel
     */
    public function setUid($uid) {
        $this->_uid = (int)$uid;

        return $this;
    }

    /**
     * 举报人id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getUid() {
        return $this->_uid;
    }

    /**
     * 对应的id (电竞圈)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $cid
     * @return \InformModel
     */
    public function setCid($cid) {
        $this->_cid = (int)$cid;

        return $this;
    }

    /**
     * 对应的id (电竞圈)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getCid() {
        return $this->_cid;
    }

    /**
     * 举报类型 1-电竞圈举报
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $type
     * @return \InformModel
     */
    public function setType($type) {
        $this->_type = (int)$type;

        return $this;
    }

    /**
     * 举报类型 1-电竞圈举报
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getType() {
        return $this->_type;
    }

    /**
     * 举报信息状态 1正常 2已处理
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $status
     * @return \InformModel
     */
    public function setStatus($status) {
        $this->_status = (int)$status;

        return $this;
    }

    /**
     * 举报信息状态 1正常 2已处理
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * 举报时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addTime
     * @return \InformModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * 举报时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'       => $this->_id,
            'mobile'   => $this->_mobile,
            'content'  => $this->_content,
            'uid'      => $this->_uid,
            'cid'      => $this->_cid,
            'type'     => $this->_type,
            'status'   => $this->_status,
            'add_time' => $this->_addTime
        );
    }

}
