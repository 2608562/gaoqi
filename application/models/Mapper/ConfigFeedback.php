<?php

namespace Mapper;

class ConfigFeedbackModel extends \Mapper\Base\AbstractModel {
    use \Base\Model\TraitModel;

    protected $table = 'config_feedback';

}
