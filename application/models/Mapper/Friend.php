<?php

namespace Mapper;

class FriendModel extends \Mapper\Base\AbstractModel {
    use \Base\Model\TraitModel;

    protected $table = 'friend';

}
