<?php

namespace Mapper;

class MemberModel extends \Mapper\Base\AbstractModel {
    use \Base\Model\TraitModel;

    protected $table = 'member';

}
