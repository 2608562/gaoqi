<?php

namespace Mapper;

class MemberAccountModel extends \Mapper\Base\AbstractModel {
    use \Base\Model\TraitModel;

    protected $table = 'member_account';

}
