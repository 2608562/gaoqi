<?php

namespace Mapper;

class OrderModel extends \Mapper\Base\AbstractModel {
    use \Base\Model\TraitModel;

    protected $table = 'order';

}
