<?php

namespace Mapper;

class OrderRefundModel extends \Mapper\Base\AbstractModel {
    use \Base\Model\TraitModel;

    protected $table = 'order_refund';

}
