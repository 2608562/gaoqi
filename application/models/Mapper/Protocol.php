<?php

namespace Mapper;

class ProtocolModel extends \Mapper\Base\AbstractModel {
    use \Base\Model\TraitModel;

    protected $table = 'protocol';

}
