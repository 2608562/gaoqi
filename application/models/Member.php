<?php

/**
 * Member
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: member
 */
class MemberModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * 手机号
     * 
     * Column Type: varchar(20)
     * 
     * @var string
     */
    protected $_mobile = '';

    /**
     * 用户密码
     * 
     * Column Type: char(43)
     * 
     * @var string
     */
    protected $_password = '';

    /**
     * 用户状态 1-正常 2禁用
     * 
     * Column Type: tinyint(3) unsigned
     * Default: 1
     * 
     * @var int
     */
    protected $_status = 1;

    /**
     * 注册时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_registTime = 0;

    /**
     * 昵称
     * 
     * Column Type: varchar(255)
     * 
     * @var string
     */
    protected $_nickName = '';

    /**
     * 性别 1男 2女
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_gender = 0;

    /**
     * 主打游戏
     * 
     * Column Type: varchar(30)
     * 
     * @var string
     */
    protected $_mainGame = '';

    /**
     * 个性签名
     * 
     * Column Type: varchar(100)
     * 
     * @var string
     */
    protected $_characterSign = '';

    /**
     * 情感状态
     * 
     * Column Type: varchar(30)
     * 
     * @var string
     */
    protected $_relationStatus = '';

    /**
     * 兴趣爱好
     * 
     * Column Type: varchar(100)
     * 
     * @var string
     */
    protected $_hobby = '';

    /**
     * 生日
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_birthday = 0;

    /**
     * 职业
     * 
     * Column Type: varchar(30)
     * 
     * @var string
     */
    protected $_profession = '';

    /**
     * 出生地
     * 
     * Column Type: varchar(30)
     * 
     * @var string
     */
    protected $_birthplace = '';

    /**
     * 外貌
     * 
     * Column Type: varchar(255)
     * 
     * @var string
     */
    protected $_appearance = '';

    /**
     * 恋爱经历
     * 
     * Column Type: varchar(30)
     * 
     * @var string
     */
    protected $_loveTimes = '';

    /**
     * 月收入
     * 
     * Column Type: varchar(30)
     * 
     * @var string
     */
    protected $_monthIncome = '';

    /**
     * 住房情况
     * 
     * Column Type: varchar(30)
     * 
     * @var string
     */
    protected $_housing = '';

    /**
     * 毕业学校
     * 
     * Column Type: varchar(50)
     * 
     * @var string
     */
    protected $_school = '';

    /**
     * 就职公司
     * 
     * Column Type: varchar(100)
     * 
     * @var string
     */
    protected $_company = '';

    /**
     * 游戏区
     * 
     * Column Type: varchar(50)
     * 
     * @var string
     */
    protected $_gameZone = '';

    /**
     * 游戏id
     * 
     * Column Type: varchar(30)
     * 
     * @var string
     */
    protected $_gameAccount = '';

    /**
     * 擅长角色
     * 
     * Column Type: varchar(30)
     * 
     * @var string
     */
    protected $_goodRole = '';

    /**
     * 搞起语录
     * 
     * Column Type: varchar(50)
     * 
     * @var string
     */
    protected $_quotations = '';

    /**
     * 其他游戏
     * 
     * Column Type: varchar(50)
     * 
     * @var string
     */
    protected $_elseGame = '';

    /**
     * 经度(最大180)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_longitude = 0;

    /**
     * 纬度(最大90)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_latitude = 0;

    /**
     * 头像
     * 
     * Column Type: varchar(255)
     * 
     * @var string
     */
    protected $_avatar = '';

    /**
     * Letter
     * 
     * Column Type: char(1)
     * 
     * @var string
     */
    protected $_letter = '';

    /**
     * 身份证号码
     * 
     * Column Type: char(18)
     * 
     * @var string
     */
    protected $_idCard = '';

    /**
     * 名字
     * 
     * Column Type: varchar(50)
     * 
     * @var string
     */
    protected $_name = '';

    /**
     * Open_id
     * 
     * Column Type: char(40)
     * 
     * @var string
     */
    protected $_openId = '';

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \MemberModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * 手机号
     * 
     * Column Type: varchar(20)
     * 
     * @param string $mobile
     * @return \MemberModel
     */
    public function setMobile($mobile) {
        $this->_mobile = (string)$mobile;

        return $this;
    }

    /**
     * 手机号
     * 
     * Column Type: varchar(20)
     * 
     * @return string
     */
    public function getMobile() {
        return $this->_mobile;
    }

    /**
     * 用户密码
     * 
     * Column Type: char(43)
     * 
     * @param string $password
     * @return \MemberModel
     */
    public function setPassword($password) {
        $this->_password = (string)$password;

        return $this;
    }

    /**
     * 用户密码
     * 
     * Column Type: char(43)
     * 
     * @return string
     */
    public function getPassword() {
        return $this->_password;
    }

    /**
     * 用户状态 1-正常 2禁用
     * 
     * Column Type: tinyint(3) unsigned
     * Default: 1
     * 
     * @param int $status
     * @return \MemberModel
     */
    public function setStatus($status) {
        $this->_status = (int)$status;

        return $this;
    }

    /**
     * 用户状态 1-正常 2禁用
     * 
     * Column Type: tinyint(3) unsigned
     * Default: 1
     * 
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * 注册时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $registTime
     * @return \MemberModel
     */
    public function setRegist_time($registTime) {
        $this->_registTime = (int)$registTime;

        return $this;
    }

    /**
     * 注册时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getRegist_time() {
        return $this->_registTime;
    }

    /**
     * 昵称
     * 
     * Column Type: varchar(255)
     * 
     * @param string $nickName
     * @return \MemberModel
     */
    public function setNick_name($nickName) {
        $this->_nickName = (string)$nickName;

        return $this;
    }

    /**
     * 昵称
     * 
     * Column Type: varchar(255)
     * 
     * @return string
     */
    public function getNick_name() {
        return $this->_nickName;
    }

    /**
     * 性别 1男 2女
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $gender
     * @return \MemberModel
     */
    public function setGender($gender) {
        $this->_gender = (int)$gender;

        return $this;
    }

    /**
     * 性别 1男 2女
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getGender() {
        return $this->_gender;
    }

    /**
     * 主打游戏
     * 
     * Column Type: varchar(30)
     * 
     * @param string $mainGame
     * @return \MemberModel
     */
    public function setMain_game($mainGame) {
        $this->_mainGame = (string)$mainGame;

        return $this;
    }

    /**
     * 主打游戏
     * 
     * Column Type: varchar(30)
     * 
     * @return string
     */
    public function getMain_game() {
        return $this->_mainGame;
    }

    /**
     * 个性签名
     * 
     * Column Type: varchar(100)
     * 
     * @param string $characterSign
     * @return \MemberModel
     */
    public function setCharacter_sign($characterSign) {
        $this->_characterSign = (string)$characterSign;

        return $this;
    }

    /**
     * 个性签名
     * 
     * Column Type: varchar(100)
     * 
     * @return string
     */
    public function getCharacter_sign() {
        return $this->_characterSign;
    }

    /**
     * 情感状态
     * 
     * Column Type: varchar(30)
     * 
     * @param string $relationStatus
     * @return \MemberModel
     */
    public function setRelation_status($relationStatus) {
        $this->_relationStatus = (string)$relationStatus;

        return $this;
    }

    /**
     * 情感状态
     * 
     * Column Type: varchar(30)
     * 
     * @return string
     */
    public function getRelation_status() {
        return $this->_relationStatus;
    }

    /**
     * 兴趣爱好
     * 
     * Column Type: varchar(100)
     * 
     * @param string $hobby
     * @return \MemberModel
     */
    public function setHobby($hobby) {
        $this->_hobby = (string)$hobby;

        return $this;
    }

    /**
     * 兴趣爱好
     * 
     * Column Type: varchar(100)
     * 
     * @return string
     */
    public function getHobby() {
        return $this->_hobby;
    }

    /**
     * 生日
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $birthday
     * @return \MemberModel
     */
    public function setBirthday($birthday) {
        $this->_birthday = (int)$birthday;

        return $this;
    }

    /**
     * 生日
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getBirthday() {
        return $this->_birthday;
    }

    /**
     * 职业
     * 
     * Column Type: varchar(30)
     * 
     * @param string $profession
     * @return \MemberModel
     */
    public function setProfession($profession) {
        $this->_profession = (string)$profession;

        return $this;
    }

    /**
     * 职业
     * 
     * Column Type: varchar(30)
     * 
     * @return string
     */
    public function getProfession() {
        return $this->_profession;
    }

    /**
     * 出生地
     * 
     * Column Type: varchar(30)
     * 
     * @param string $birthplace
     * @return \MemberModel
     */
    public function setBirthplace($birthplace) {
        $this->_birthplace = (string)$birthplace;

        return $this;
    }

    /**
     * 出生地
     * 
     * Column Type: varchar(30)
     * 
     * @return string
     */
    public function getBirthplace() {
        return $this->_birthplace;
    }

    /**
     * 外貌
     * 
     * Column Type: varchar(255)
     * 
     * @param string $appearance
     * @return \MemberModel
     */
    public function setAppearance($appearance) {
        $this->_appearance = (string)$appearance;

        return $this;
    }

    /**
     * 外貌
     * 
     * Column Type: varchar(255)
     * 
     * @return string
     */
    public function getAppearance() {
        return $this->_appearance;
    }

    /**
     * 恋爱经历
     * 
     * Column Type: varchar(30)
     * 
     * @param string $loveTimes
     * @return \MemberModel
     */
    public function setLove_times($loveTimes) {
        $this->_loveTimes = (string)$loveTimes;

        return $this;
    }

    /**
     * 恋爱经历
     * 
     * Column Type: varchar(30)
     * 
     * @return string
     */
    public function getLove_times() {
        return $this->_loveTimes;
    }

    /**
     * 月收入
     * 
     * Column Type: varchar(30)
     * 
     * @param string $monthIncome
     * @return \MemberModel
     */
    public function setMonth_income($monthIncome) {
        $this->_monthIncome = (string)$monthIncome;

        return $this;
    }

    /**
     * 月收入
     * 
     * Column Type: varchar(30)
     * 
     * @return string
     */
    public function getMonth_income() {
        return $this->_monthIncome;
    }

    /**
     * 住房情况
     * 
     * Column Type: varchar(30)
     * 
     * @param string $housing
     * @return \MemberModel
     */
    public function setHousing($housing) {
        $this->_housing = (string)$housing;

        return $this;
    }

    /**
     * 住房情况
     * 
     * Column Type: varchar(30)
     * 
     * @return string
     */
    public function getHousing() {
        return $this->_housing;
    }

    /**
     * 毕业学校
     * 
     * Column Type: varchar(50)
     * 
     * @param string $school
     * @return \MemberModel
     */
    public function setSchool($school) {
        $this->_school = (string)$school;

        return $this;
    }

    /**
     * 毕业学校
     * 
     * Column Type: varchar(50)
     * 
     * @return string
     */
    public function getSchool() {
        return $this->_school;
    }

    /**
     * 就职公司
     * 
     * Column Type: varchar(100)
     * 
     * @param string $company
     * @return \MemberModel
     */
    public function setCompany($company) {
        $this->_company = (string)$company;

        return $this;
    }

    /**
     * 就职公司
     * 
     * Column Type: varchar(100)
     * 
     * @return string
     */
    public function getCompany() {
        return $this->_company;
    }

    /**
     * 游戏区
     * 
     * Column Type: varchar(50)
     * 
     * @param string $gameZone
     * @return \MemberModel
     */
    public function setGame_zone($gameZone) {
        $this->_gameZone = (string)$gameZone;

        return $this;
    }

    /**
     * 游戏区
     * 
     * Column Type: varchar(50)
     * 
     * @return string
     */
    public function getGame_zone() {
        return $this->_gameZone;
    }

    /**
     * 游戏id
     * 
     * Column Type: varchar(30)
     * 
     * @param string $gameAccount
     * @return \MemberModel
     */
    public function setGame_account($gameAccount) {
        $this->_gameAccount = (string)$gameAccount;

        return $this;
    }

    /**
     * 游戏id
     * 
     * Column Type: varchar(30)
     * 
     * @return string
     */
    public function getGame_account() {
        return $this->_gameAccount;
    }

    /**
     * 擅长角色
     * 
     * Column Type: varchar(30)
     * 
     * @param string $goodRole
     * @return \MemberModel
     */
    public function setGood_role($goodRole) {
        $this->_goodRole = (string)$goodRole;

        return $this;
    }

    /**
     * 擅长角色
     * 
     * Column Type: varchar(30)
     * 
     * @return string
     */
    public function getGood_role() {
        return $this->_goodRole;
    }

    /**
     * 搞起语录
     * 
     * Column Type: varchar(50)
     * 
     * @param string $quotations
     * @return \MemberModel
     */
    public function setQuotations($quotations) {
        $this->_quotations = (string)$quotations;

        return $this;
    }

    /**
     * 搞起语录
     * 
     * Column Type: varchar(50)
     * 
     * @return string
     */
    public function getQuotations() {
        return $this->_quotations;
    }

    /**
     * 其他游戏
     * 
     * Column Type: varchar(50)
     * 
     * @param string $elseGame
     * @return \MemberModel
     */
    public function setElse_game($elseGame) {
        $this->_elseGame = (string)$elseGame;

        return $this;
    }

    /**
     * 其他游戏
     * 
     * Column Type: varchar(50)
     * 
     * @return string
     */
    public function getElse_game() {
        return $this->_elseGame;
    }

    /**
     * 经度(最大180)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $longitude
     * @return \MemberModel
     */
    public function setLongitude($longitude) {
        $this->_longitude = (int)$longitude;

        return $this;
    }

    /**
     * 经度(最大180)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getLongitude() {
        return $this->_longitude;
    }

    /**
     * 纬度(最大90)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $latitude
     * @return \MemberModel
     */
    public function setLatitude($latitude) {
        $this->_latitude = (int)$latitude;

        return $this;
    }

    /**
     * 纬度(最大90)
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getLatitude() {
        return $this->_latitude;
    }

    /**
     * 头像
     * 
     * Column Type: varchar(255)
     * 
     * @param string $avatar
     * @return \MemberModel
     */
    public function setAvatar($avatar) {
        $this->_avatar = (string)$avatar;

        return $this;
    }

    /**
     * 头像
     * 
     * Column Type: varchar(255)
     * 
     * @return string
     */
    public function getAvatar() {
        return $this->_avatar;
    }

    /**
     * Letter
     * 
     * Column Type: char(1)
     * 
     * @param string $letter
     * @return \MemberModel
     */
    public function setLetter($letter) {
        $this->_letter = (string)$letter;

        return $this;
    }

    /**
     * Letter
     * 
     * Column Type: char(1)
     * 
     * @return string
     */
    public function getLetter() {
        return $this->_letter;
    }

    /**
     * 身份证号码
     * 
     * Column Type: char(18)
     * 
     * @param string $idCard
     * @return \MemberModel
     */
    public function setId_card($idCard) {
        $this->_idCard = (string)$idCard;

        return $this;
    }

    /**
     * 身份证号码
     * 
     * Column Type: char(18)
     * 
     * @return string
     */
    public function getId_card() {
        return $this->_idCard;
    }

    /**
     * 名字
     * 
     * Column Type: varchar(50)
     * 
     * @param string $name
     * @return \MemberModel
     */
    public function setName($name) {
        $this->_name = (string)$name;

        return $this;
    }

    /**
     * 名字
     * 
     * Column Type: varchar(50)
     * 
     * @return string
     */
    public function getName() {
        return $this->_name;
    }

    /**
     * Open_id
     * 
     * Column Type: char(40)
     * 
     * @param string $openId
     * @return \MemberModel
     */
    public function setOpen_id($openId) {
        $this->_openId = (string)$openId;

        return $this;
    }

    /**
     * Open_id
     * 
     * Column Type: char(40)
     * 
     * @return string
     */
    public function getOpen_id() {
        return $this->_openId;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'              => $this->_id,
            'mobile'          => $this->_mobile,
            'password'        => $this->_password,
            'status'          => $this->_status,
            'regist_time'     => $this->_registTime,
            'nick_name'       => $this->_nickName,
            'gender'          => $this->_gender,
            'main_game'       => $this->_mainGame,
            'character_sign'  => $this->_characterSign,
            'relation_status' => $this->_relationStatus,
            'hobby'           => $this->_hobby,
            'birthday'        => $this->_birthday,
            'profession'      => $this->_profession,
            'birthplace'      => $this->_birthplace,
            'appearance'      => $this->_appearance,
            'love_times'      => $this->_loveTimes,
            'month_income'    => $this->_monthIncome,
            'housing'         => $this->_housing,
            'school'          => $this->_school,
            'company'         => $this->_company,
            'game_zone'       => $this->_gameZone,
            'game_account'    => $this->_gameAccount,
            'good_role'       => $this->_goodRole,
            'quotations'      => $this->_quotations,
            'else_game'       => $this->_elseGame,
            'longitude'       => $this->_longitude,
            'latitude'        => $this->_latitude,
            'avatar'          => $this->_avatar,
            'letter'          => $this->_letter,
            'id_card'         => $this->_idCard,
            'name'            => $this->_name,
            'open_id'         => $this->_openId
        );
    }

}
