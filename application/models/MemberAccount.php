<?php

/**
 * Member_account
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: member_account
 */
class MemberAccountModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * 会员ID
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_uid = 0;

    /**
     * 帐号余额(余额=总额-已使用-提现中的金额)
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @var float
     */
    protected $_nowSum = 0.00;

    /**
     * 已使用
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @var float
     */
    protected $_usedSum = 0.00;

    /**
     * 提现中的总金额
     * 
     * Column Type: decimal(10,2)
     * Default: 0.00
     * 
     * @var float
     */
    protected $_withdrawSum = 0.00;

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_updateTime = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \MemberAccountModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * 会员ID
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $uid
     * @return \MemberAccountModel
     */
    public function setUid($uid) {
        $this->_uid = (int)$uid;

        return $this;
    }

    /**
     * 会员ID
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getUid() {
        return $this->_uid;
    }

    /**
     * 帐号余额(余额=总额-已使用-提现中的金额)
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @param float $nowSum
     * @return \MemberAccountModel
     */
    public function setNow_sum($nowSum) {
        $this->_nowSum = (float)$nowSum;

        return $this;
    }

    /**
     * 帐号余额(余额=总额-已使用-提现中的金额)
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @return float
     */
    public function getNow_sum() {
        return $this->_nowSum;
    }

    /**
     * 已使用
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @param float $usedSum
     * @return \MemberAccountModel
     */
    public function setUsed_sum($usedSum) {
        $this->_usedSum = (float)$usedSum;

        return $this;
    }

    /**
     * 已使用
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @return float
     */
    public function getUsed_sum() {
        return $this->_usedSum;
    }

    /**
     * 提现中的总金额
     * 
     * Column Type: decimal(10,2)
     * Default: 0.00
     * 
     * @param float $withdrawSum
     * @return \MemberAccountModel
     */
    public function setWithdraw_sum($withdrawSum) {
        $this->_withdrawSum = (float)$withdrawSum;

        return $this;
    }

    /**
     * 提现中的总金额
     * 
     * Column Type: decimal(10,2)
     * Default: 0.00
     * 
     * @return float
     */
    public function getWithdraw_sum() {
        return $this->_withdrawSum;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addTime
     * @return \MemberAccountModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $updateTime
     * @return \MemberAccountModel
     */
    public function setUpdate_time($updateTime) {
        $this->_updateTime = (int)$updateTime;

        return $this;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getUpdate_time() {
        return $this->_updateTime;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'           => $this->_id,
            'uid'          => $this->_uid,
            'now_sum'      => $this->_nowSum,
            'used_sum'     => $this->_usedSum,
            'withdraw_sum' => $this->_withdrawSum,
            'add_time'     => $this->_addTime,
            'update_time'  => $this->_updateTime
        );
    }

}
