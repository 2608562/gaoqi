<?php

/**
 * 会员 多头像
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: member_avatar
 */
class MemberAvatarModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * Uid
     * 
     * Column Type: int(10) unsigned
     * MUL
     * 
     * @var int
     */
    protected $_uid = null;

    /**
     * Order
     * 
     * Column Type: int(10) unsigned
     * 
     * @var int
     */
    protected $_order = null;

    /**
     * Src
     * 
     * Column Type: varchar(255)
     * 
     * @var string
     */
    protected $_src = null;

    /**
     * Addtime
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addtime = 0;

    /**
     * Lasttime
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_lasttime = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \MemberAvatarModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * Uid
     * 
     * Column Type: int(10) unsigned
     * MUL
     * 
     * @param int $uid
     * @return \MemberAvatarModel
     */
    public function setUid($uid) {
        $this->_uid = (int)$uid;

        return $this;
    }

    /**
     * Uid
     * 
     * Column Type: int(10) unsigned
     * MUL
     * 
     * @return int
     */
    public function getUid() {
        return $this->_uid;
    }

    /**
     * Order
     * 
     * Column Type: int(10) unsigned
     * 
     * @param int $order
     * @return \MemberAvatarModel
     */
    public function setOrder($order) {
        $this->_order = (int)$order;

        return $this;
    }

    /**
     * Order
     * 
     * Column Type: int(10) unsigned
     * 
     * @return int
     */
    public function getOrder() {
        return $this->_order;
    }

    /**
     * Src
     * 
     * Column Type: varchar(255)
     * 
     * @param string $src
     * @return \MemberAvatarModel
     */
    public function setSrc($src) {
        $this->_src = (string)$src;

        return $this;
    }

    /**
     * Src
     * 
     * Column Type: varchar(255)
     * 
     * @return string
     */
    public function getSrc() {
        return $this->_src;
    }

    /**
     * Addtime
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addtime
     * @return \MemberAvatarModel
     */
    public function setAddtime($addtime) {
        $this->_addtime = (int)$addtime;

        return $this;
    }

    /**
     * Addtime
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAddtime() {
        return $this->_addtime;
    }

    /**
     * Lasttime
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $lasttime
     * @return \MemberAvatarModel
     */
    public function setLasttime($lasttime) {
        $this->_lasttime = (int)$lasttime;

        return $this;
    }

    /**
     * Lasttime
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getLasttime() {
        return $this->_lasttime;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'       => $this->_id,
            'uid'      => $this->_uid,
            'order'    => $this->_order,
            'src'      => $this->_src,
            'addtime'  => $this->_addtime,
            'lasttime' => $this->_lasttime
        );
    }

}
