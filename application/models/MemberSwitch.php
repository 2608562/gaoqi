<?php

/**
 * Member_switch
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: member_switch
 */
class MemberSwitchModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * 用户的id
     * 
     * Column Type: int(10) unsigned
     * 
     * @var int
     */
    protected $_uid = null;

    /**
     * 1-已经认证
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_authentication = 0;

    /**
     * 1-接收新消息  2-不接收新消息
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_receiverInform = 0;

    /**
     * 1-显示发信人和摘要  2-只显示发信人隐藏聊天正文
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_informType = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \MemberSwitchModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * 用户的id
     * 
     * Column Type: int(10) unsigned
     * 
     * @param int $uid
     * @return \MemberSwitchModel
     */
    public function setUid($uid) {
        $this->_uid = (int)$uid;

        return $this;
    }

    /**
     * 用户的id
     * 
     * Column Type: int(10) unsigned
     * 
     * @return int
     */
    public function getUid() {
        return $this->_uid;
    }

    /**
     * 1-已经认证
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $authentication
     * @return \MemberSwitchModel
     */
    public function setAuthentication($authentication) {
        $this->_authentication = (int)$authentication;

        return $this;
    }

    /**
     * 1-已经认证
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAuthentication() {
        return $this->_authentication;
    }

    /**
     * 1-接收新消息  2-不接收新消息
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $receiverInform
     * @return \MemberSwitchModel
     */
    public function setReceiver_inform($receiverInform) {
        $this->_receiverInform = (int)$receiverInform;

        return $this;
    }

    /**
     * 1-接收新消息  2-不接收新消息
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getReceiver_inform() {
        return $this->_receiverInform;
    }

    /**
     * 1-显示发信人和摘要  2-只显示发信人隐藏聊天正文
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $informType
     * @return \MemberSwitchModel
     */
    public function setInform_type($informType) {
        $this->_informType = (int)$informType;

        return $this;
    }

    /**
     * 1-显示发信人和摘要  2-只显示发信人隐藏聊天正文
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getInform_type() {
        return $this->_informType;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'              => $this->_id,
            'uid'             => $this->_uid,
            'authentication'  => $this->_authentication,
            'receiver_inform' => $this->_receiverInform,
            'inform_type'     => $this->_informType
        );
    }

}
