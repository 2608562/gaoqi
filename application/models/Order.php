<?php

/**
 * Order
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: order
 */
class OrderModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * From_uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_fromUid = 0;

    /**
     * To_uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_toUid = 0;

    /**
     * 类型 1-看大腿 2求大腿
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_type = 0;

    /**
     * 1-待支付 2-已支付 3-退款中 4-拒绝退款 5-已退款 6-已完成 7-已评价 8-已关闭  9-删除状态
     * 
     * Column Type: tinyint(1)
     * Default: 0
     * 
     * @var int
     */
    protected $_status = 0;

    /**
     * 陪练信息id
     * 
     * Column Type: varchar(50)
     * Default: 0
     * 
     * @var string
     */
    protected $_sparringNo = 0;

    /**
     * 红包id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_envelopeId = 0;

    /**
     * 原价
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @var float
     */
    protected $_price = 0.00;

    /**
     * 支付价格
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @var float
     */
    protected $_payPrice = 0.00;

    /**
     * 订单号
     * 
     * Column Type: char(16)
     * 
     * @var string
     */
    protected $_orderNo = '';

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_updateTime = 0;

    /**
     * 备注
     * 
     * Column Type: varchar(50)
     * 
     * @var string
     */
    protected $_remark = '';

    /**
     * 时长(求大腿使用)
     * 
     * Column Type: tinyint(2)
     * Default: 0
     * 
     * @var int
     */
    protected $_duration = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \OrderModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * From_uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $fromUid
     * @return \OrderModel
     */
    public function setFrom_uid($fromUid) {
        $this->_fromUid = (int)$fromUid;

        return $this;
    }

    /**
     * From_uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getFrom_uid() {
        return $this->_fromUid;
    }

    /**
     * To_uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $toUid
     * @return \OrderModel
     */
    public function setTo_uid($toUid) {
        $this->_toUid = (int)$toUid;

        return $this;
    }

    /**
     * To_uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getTo_uid() {
        return $this->_toUid;
    }

    /**
     * 类型 1-看大腿 2求大腿
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $type
     * @return \OrderModel
     */
    public function setType($type) {
        $this->_type = (int)$type;

        return $this;
    }

    /**
     * 类型 1-看大腿 2求大腿
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getType() {
        return $this->_type;
    }

    /**
     * 1-待支付 2-已支付 3-退款中 4-拒绝退款 5-已退款 6-已完成 7-已评价 8-已关闭  9-删除状态
     * 
     * Column Type: tinyint(1)
     * Default: 0
     * 
     * @param int $status
     * @return \OrderModel
     */
    public function setStatus($status) {
        $this->_status = (int)$status;

        return $this;
    }

    /**
     * 1-待支付 2-已支付 3-退款中 4-拒绝退款 5-已退款 6-已完成 7-已评价 8-已关闭  9-删除状态
     * 
     * Column Type: tinyint(1)
     * Default: 0
     * 
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * 陪练信息id
     * 
     * Column Type: varchar(50)
     * Default: 0
     * 
     * @param string $sparringNo
     * @return \OrderModel
     */
    public function setSparring_no($sparringNo) {
        $this->_sparringNo = (string)$sparringNo;

        return $this;
    }

    /**
     * 陪练信息id
     * 
     * Column Type: varchar(50)
     * Default: 0
     * 
     * @return string
     */
    public function getSparring_no() {
        return $this->_sparringNo;
    }

    /**
     * 红包id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $envelopeId
     * @return \OrderModel
     */
    public function setEnvelope_id($envelopeId) {
        $this->_envelopeId = (int)$envelopeId;

        return $this;
    }

    /**
     * 红包id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getEnvelope_id() {
        return $this->_envelopeId;
    }

    /**
     * 原价
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @param float $price
     * @return \OrderModel
     */
    public function setPrice($price) {
        $this->_price = (float)$price;

        return $this;
    }

    /**
     * 原价
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @return float
     */
    public function getPrice() {
        return $this->_price;
    }

    /**
     * 支付价格
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @param float $payPrice
     * @return \OrderModel
     */
    public function setPay_price($payPrice) {
        $this->_payPrice = (float)$payPrice;

        return $this;
    }

    /**
     * 支付价格
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @return float
     */
    public function getPay_price() {
        return $this->_payPrice;
    }

    /**
     * 订单号
     * 
     * Column Type: char(16)
     * 
     * @param string $orderNo
     * @return \OrderModel
     */
    public function setOrder_no($orderNo) {
        $this->_orderNo = (string)$orderNo;

        return $this;
    }

    /**
     * 订单号
     * 
     * Column Type: char(16)
     * 
     * @return string
     */
    public function getOrder_no() {
        return $this->_orderNo;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addTime
     * @return \OrderModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $updateTime
     * @return \OrderModel
     */
    public function setUpdate_time($updateTime) {
        $this->_updateTime = (int)$updateTime;

        return $this;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getUpdate_time() {
        return $this->_updateTime;
    }

    /**
     * 备注
     * 
     * Column Type: varchar(50)
     * 
     * @param string $remark
     * @return \OrderModel
     */
    public function setRemark($remark) {
        $this->_remark = (string)$remark;

        return $this;
    }

    /**
     * 备注
     * 
     * Column Type: varchar(50)
     * 
     * @return string
     */
    public function getRemark() {
        return $this->_remark;
    }

    /**
     * 时长(求大腿使用)
     * 
     * Column Type: tinyint(2)
     * Default: 0
     * 
     * @param int $duration
     * @return \OrderModel
     */
    public function setDuration($duration) {
        $this->_duration = (int)$duration;

        return $this;
    }

    /**
     * 时长(求大腿使用)
     * 
     * Column Type: tinyint(2)
     * Default: 0
     * 
     * @return int
     */
    public function getDuration() {
        return $this->_duration;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'          => $this->_id,
            'from_uid'    => $this->_fromUid,
            'to_uid'      => $this->_toUid,
            'type'        => $this->_type,
            'status'      => $this->_status,
            'sparring_no' => $this->_sparringNo,
            'envelope_id' => $this->_envelopeId,
            'price'       => $this->_price,
            'pay_price'   => $this->_payPrice,
            'order_no'    => $this->_orderNo,
            'add_time'    => $this->_addTime,
            'update_time' => $this->_updateTime,
            'remark'      => $this->_remark,
            'duration'    => $this->_duration
        );
    }

}
