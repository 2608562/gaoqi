<?php

/**
 * Order_pay
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: order_pay
 */
class OrderPayModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * Uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_uid = 0;

    /**
     * Order_id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_orderId = 0;

    /**
     * Pay_price
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @var float
     */
    protected $_payPrice = 0.00;

    /**
     * Transaction_id
     * 
     * Column Type: varchar(28)
     * 
     * @var string
     */
    protected $_transactionId = null;

    /**
     * Type
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_type = 0;

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \OrderPayModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * Uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $uid
     * @return \OrderPayModel
     */
    public function setUid($uid) {
        $this->_uid = (int)$uid;

        return $this;
    }

    /**
     * Uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getUid() {
        return $this->_uid;
    }

    /**
     * Order_id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $orderId
     * @return \OrderPayModel
     */
    public function setOrder_id($orderId) {
        $this->_orderId = (int)$orderId;

        return $this;
    }

    /**
     * Order_id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getOrder_id() {
        return $this->_orderId;
    }

    /**
     * Pay_price
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @param float $payPrice
     * @return \OrderPayModel
     */
    public function setPay_price($payPrice) {
        $this->_payPrice = (float)$payPrice;

        return $this;
    }

    /**
     * Pay_price
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @return float
     */
    public function getPay_price() {
        return $this->_payPrice;
    }

    /**
     * Transaction_id
     * 
     * Column Type: varchar(28)
     * 
     * @param string $transactionId
     * @return \OrderPayModel
     */
    public function setTransaction_id($transactionId) {
        $this->_transactionId = (string)$transactionId;

        return $this;
    }

    /**
     * Transaction_id
     * 
     * Column Type: varchar(28)
     * 
     * @return string
     */
    public function getTransaction_id() {
        return $this->_transactionId;
    }

    /**
     * Type
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $type
     * @return \OrderPayModel
     */
    public function setType($type) {
        $this->_type = (int)$type;

        return $this;
    }

    /**
     * Type
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getType() {
        return $this->_type;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addTime
     * @return \OrderPayModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'             => $this->_id,
            'uid'            => $this->_uid,
            'order_id'       => $this->_orderId,
            'pay_price'      => $this->_payPrice,
            'transaction_id' => $this->_transactionId,
            'type'           => $this->_type,
            'add_time'       => $this->_addTime
        );
    }

}
