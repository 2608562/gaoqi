<?php

/**
 * Order_refund
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: order_refund
 */
class OrderRefundModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * Sum
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @var float
     */
    protected $_sum = 0.00;

    /**
     * Reason
     * 
     * Column Type: varchar(255)
     * 
     * @var string
     */
    protected $_reason = '';

    /**
     * Order_id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_orderId = 0;

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_updateTime = 0;

    /**
     * Status
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_status = 0;

    /**
     * Remark
     * 
     * Column Type: varchar(255)
     * 
     * @var string
     */
    protected $_remark = '';

    /**
     * Refuse_reason
     * 
     * Column Type: varchar(255)
     * 
     * @var string
     */
    protected $_refuseReason = null;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \OrderRefundModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * Sum
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @param float $sum
     * @return \OrderRefundModel
     */
    public function setSum($sum) {
        $this->_sum = (float)$sum;

        return $this;
    }

    /**
     * Sum
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @return float
     */
    public function getSum() {
        return $this->_sum;
    }

    /**
     * Reason
     * 
     * Column Type: varchar(255)
     * 
     * @param string $reason
     * @return \OrderRefundModel
     */
    public function setReason($reason) {
        $this->_reason = (string)$reason;

        return $this;
    }

    /**
     * Reason
     * 
     * Column Type: varchar(255)
     * 
     * @return string
     */
    public function getReason() {
        return $this->_reason;
    }

    /**
     * Order_id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $orderId
     * @return \OrderRefundModel
     */
    public function setOrder_id($orderId) {
        $this->_orderId = (int)$orderId;

        return $this;
    }

    /**
     * Order_id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getOrder_id() {
        return $this->_orderId;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addTime
     * @return \OrderRefundModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $updateTime
     * @return \OrderRefundModel
     */
    public function setUpdate_time($updateTime) {
        $this->_updateTime = (int)$updateTime;

        return $this;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getUpdate_time() {
        return $this->_updateTime;
    }

    /**
     * Status
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $status
     * @return \OrderRefundModel
     */
    public function setStatus($status) {
        $this->_status = (int)$status;

        return $this;
    }

    /**
     * Status
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * Remark
     * 
     * Column Type: varchar(255)
     * 
     * @param string $remark
     * @return \OrderRefundModel
     */
    public function setRemark($remark) {
        $this->_remark = (string)$remark;

        return $this;
    }

    /**
     * Remark
     * 
     * Column Type: varchar(255)
     * 
     * @return string
     */
    public function getRemark() {
        return $this->_remark;
    }

    /**
     * Refuse_reason
     * 
     * Column Type: varchar(255)
     * 
     * @param string $refuseReason
     * @return \OrderRefundModel
     */
    public function setRefuse_reason($refuseReason) {
        $this->_refuseReason = (string)$refuseReason;

        return $this;
    }

    /**
     * Refuse_reason
     * 
     * Column Type: varchar(255)
     * 
     * @return string
     */
    public function getRefuse_reason() {
        return $this->_refuseReason;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'            => $this->_id,
            'sum'           => $this->_sum,
            'reason'        => $this->_reason,
            'order_id'      => $this->_orderId,
            'add_time'      => $this->_addTime,
            'update_time'   => $this->_updateTime,
            'status'        => $this->_status,
            'remark'        => $this->_remark,
            'refuse_reason' => $this->_refuseReason
        );
    }

}
