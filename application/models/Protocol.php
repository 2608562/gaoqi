<?php

/**
 * Protocol
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: protocol
 */
class ProtocolModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * 1-app协议 2-建群协议 3-陪练协议 4-隐私协议
     * 
     * Column Type: varchar(8)
     * 
     * @var string
     */
    protected $_type = null;

    /**
     * 协议标题
     * 
     * Column Type: char(255)
     * 
     * @var string
     */
    protected $_title = null;

    /**
     * 协议内容
     * 
     * Column Type: text
     * 
     * @var string
     */
    protected $_content = null;

    /**
     * 更新时间
     * 
     * Column Type: int(10) unsigned
     * 
     * @var int
     */
    protected $_updateTime = null;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \ProtocolModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * 1-app协议 2-建群协议 3-陪练协议 4-隐私协议
     * 
     * Column Type: varchar(8)
     * 
     * @param string $type
     * @return \ProtocolModel
     */
    public function setType($type) {
        $this->_type = (string)$type;

        return $this;
    }

    /**
     * 1-app协议 2-建群协议 3-陪练协议 4-隐私协议
     * 
     * Column Type: varchar(8)
     * 
     * @return string
     */
    public function getType() {
        return $this->_type;
    }

    /**
     * 协议标题
     * 
     * Column Type: char(255)
     * 
     * @param string $title
     * @return \ProtocolModel
     */
    public function setTitle($title) {
        $this->_title = (string)$title;

        return $this;
    }

    /**
     * 协议标题
     * 
     * Column Type: char(255)
     * 
     * @return string
     */
    public function getTitle() {
        return $this->_title;
    }

    /**
     * 协议内容
     * 
     * Column Type: text
     * 
     * @param string $content
     * @return \ProtocolModel
     */
    public function setContent($content) {
        $this->_content = (string)$content;

        return $this;
    }

    /**
     * 协议内容
     * 
     * Column Type: text
     * 
     * @return string
     */
    public function getContent() {
        return $this->_content;
    }

    /**
     * 更新时间
     * 
     * Column Type: int(10) unsigned
     * 
     * @param int $updateTime
     * @return \ProtocolModel
     */
    public function setUpdate_time($updateTime) {
        $this->_updateTime = (int)$updateTime;

        return $this;
    }

    /**
     * 更新时间
     * 
     * Column Type: int(10) unsigned
     * 
     * @return int
     */
    public function getUpdate_time() {
        return $this->_updateTime;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'          => $this->_id,
            'type'        => $this->_type,
            'title'       => $this->_title,
            'content'     => $this->_content,
            'update_time' => $this->_updateTime
        );
    }

}
