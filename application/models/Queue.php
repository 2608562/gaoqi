<?php

/**
 * 队列 
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: queue
 */
class QueueModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * Type
     * 
     * Column Type: enum('message','sms','email')
     * Default: message
     * 
     * @var string
     */
    protected $_type = 'message';

    /**
     * JSON格式内容数据
     * 
     * Column Type: varchar(2048)
     * 
     * @var string
     */
    protected $_content = null;

    /**
     * Status
     * 
     * Column Type: enum('pending','processing','processed','failed','successed')
     * Default: pending
     * 
     * @var string
     */
    protected $_status = 'pending';

    /**
     * Time
     * 
     * Column Type: int(10)
     * Default: 0
     * 
     * @var int
     */
    protected $_time = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \QueueModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * Type
     * 
     * Column Type: enum('message','sms','email')
     * Default: message
     * 
     * @param string $type
     * @return \QueueModel
     */
    public function setType($type) {
        $this->_type = (string)$type;

        return $this;
    }

    /**
     * Type
     * 
     * Column Type: enum('message','sms','email')
     * Default: message
     * 
     * @return string
     */
    public function getType() {
        return $this->_type;
    }

    /**
     * JSON格式内容数据
     * 
     * Column Type: varchar(2048)
     * 
     * @param string $content
     * @return \QueueModel
     */
    public function setContent($content) {
        $this->_content = (string)$content;

        return $this;
    }

    /**
     * JSON格式内容数据
     * 
     * Column Type: varchar(2048)
     * 
     * @return string
     */
    public function getContent() {
        return $this->_content;
    }

    /**
     * Status
     * 
     * Column Type: enum('pending','processing','processed','failed','successed')
     * Default: pending
     * 
     * @param string $status
     * @return \QueueModel
     */
    public function setStatus($status) {
        $this->_status = (string)$status;

        return $this;
    }

    /**
     * Status
     * 
     * Column Type: enum('pending','processing','processed','failed','successed')
     * Default: pending
     * 
     * @return string
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * Time
     * 
     * Column Type: int(10)
     * Default: 0
     * 
     * @param int $time
     * @return \QueueModel
     */
    public function setTime($time) {
        $this->_time = (int)$time;

        return $this;
    }

    /**
     * Time
     * 
     * Column Type: int(10)
     * Default: 0
     * 
     * @return int
     */
    public function getTime() {
        return $this->_time;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'      => $this->_id,
            'type'    => $this->_type,
            'content' => $this->_content,
            'status'  => $this->_status,
            'time'    => $this->_time
        );
    }

}
