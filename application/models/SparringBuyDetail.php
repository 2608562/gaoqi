<?php

/**
 * Sparring_buy_detail
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: sparring_buy_detail
 */
class SparringBuyDetailModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * 用户id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_sparringBuyId = 0;

    /**
     * Game_id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_gameId = 0;

    /**
     * 指定网吧
     * 
     * Column Type: varchar(50)
     * 
     * @var string
     */
    protected $_netBar = '';

    /**
     * 价格
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @var float
     */
    protected $_price = 0.00;

    /**
     * 空闲时间-开始
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_beginTime = 0;

    /**
     * 空闲时间-结束
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_endTime = 0;

    /**
     * 游戏水平
     * 
     * Column Type: varchar(30)
     * 
     * @var string
     */
    protected $_gameLevel = '';

    /**
     * 游戏服务器/平台
     * 
     * Column Type: varchar(30)
     * 
     * @var string
     */
    protected $_gameServer = '';

    /**
     * 搞起宣言
     * 
     * Column Type: varchar(100)
     * 
     * @var string
     */
    protected $_manifesto = '';

    /**
     * Gender
     * 
     * Column Type: tinyint(1) unsigned
     * 
     * @var int
     */
    protected $_gender = null;

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * 修改时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_updateTime = 0;

    /**
     * 状态值 1正常 2已被购买 3删除
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_status = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \SparringBuyDetailModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * 用户id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $sparringBuyId
     * @return \SparringBuyDetailModel
     */
    public function setSparring_buy_id($sparringBuyId) {
        $this->_sparringBuyId = (int)$sparringBuyId;

        return $this;
    }

    /**
     * 用户id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getSparring_buy_id() {
        return $this->_sparringBuyId;
    }

    /**
     * Game_id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $gameId
     * @return \SparringBuyDetailModel
     */
    public function setGame_id($gameId) {
        $this->_gameId = (int)$gameId;

        return $this;
    }

    /**
     * Game_id
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getGame_id() {
        return $this->_gameId;
    }

    /**
     * 指定网吧
     * 
     * Column Type: varchar(50)
     * 
     * @param string $netBar
     * @return \SparringBuyDetailModel
     */
    public function setNet_bar($netBar) {
        $this->_netBar = (string)$netBar;

        return $this;
    }

    /**
     * 指定网吧
     * 
     * Column Type: varchar(50)
     * 
     * @return string
     */
    public function getNet_bar() {
        return $this->_netBar;
    }

    /**
     * 价格
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @param float $price
     * @return \SparringBuyDetailModel
     */
    public function setPrice($price) {
        $this->_price = (float)$price;

        return $this;
    }

    /**
     * 价格
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @return float
     */
    public function getPrice() {
        return $this->_price;
    }

    /**
     * 空闲时间-开始
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $beginTime
     * @return \SparringBuyDetailModel
     */
    public function setBegin_time($beginTime) {
        $this->_beginTime = (int)$beginTime;

        return $this;
    }

    /**
     * 空闲时间-开始
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getBegin_time() {
        return $this->_beginTime;
    }

    /**
     * 空闲时间-结束
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $endTime
     * @return \SparringBuyDetailModel
     */
    public function setEnd_time($endTime) {
        $this->_endTime = (int)$endTime;

        return $this;
    }

    /**
     * 空闲时间-结束
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getEnd_time() {
        return $this->_endTime;
    }

    /**
     * 游戏水平
     * 
     * Column Type: varchar(30)
     * 
     * @param string $gameLevel
     * @return \SparringBuyDetailModel
     */
    public function setGame_level($gameLevel) {
        $this->_gameLevel = (string)$gameLevel;

        return $this;
    }

    /**
     * 游戏水平
     * 
     * Column Type: varchar(30)
     * 
     * @return string
     */
    public function getGame_level() {
        return $this->_gameLevel;
    }

    /**
     * 游戏服务器/平台
     * 
     * Column Type: varchar(30)
     * 
     * @param string $gameServer
     * @return \SparringBuyDetailModel
     */
    public function setGame_server($gameServer) {
        $this->_gameServer = (string)$gameServer;

        return $this;
    }

    /**
     * 游戏服务器/平台
     * 
     * Column Type: varchar(30)
     * 
     * @return string
     */
    public function getGame_server() {
        return $this->_gameServer;
    }

    /**
     * 搞起宣言
     * 
     * Column Type: varchar(100)
     * 
     * @param string $manifesto
     * @return \SparringBuyDetailModel
     */
    public function setManifesto($manifesto) {
        $this->_manifesto = (string)$manifesto;

        return $this;
    }

    /**
     * 搞起宣言
     * 
     * Column Type: varchar(100)
     * 
     * @return string
     */
    public function getManifesto() {
        return $this->_manifesto;
    }

    /**
     * Gender
     * 
     * Column Type: tinyint(1) unsigned
     * 
     * @param int $gender
     * @return \SparringBuyDetailModel
     */
    public function setGender($gender) {
        $this->_gender = (int)$gender;

        return $this;
    }

    /**
     * Gender
     * 
     * Column Type: tinyint(1) unsigned
     * 
     * @return int
     */
    public function getGender() {
        return $this->_gender;
    }

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addTime
     * @return \SparringBuyDetailModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * 添加时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * 修改时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $updateTime
     * @return \SparringBuyDetailModel
     */
    public function setUpdate_time($updateTime) {
        $this->_updateTime = (int)$updateTime;

        return $this;
    }

    /**
     * 修改时间
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getUpdate_time() {
        return $this->_updateTime;
    }

    /**
     * 状态值 1正常 2已被购买 3删除
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $status
     * @return \SparringBuyDetailModel
     */
    public function setStatus($status) {
        $this->_status = (int)$status;

        return $this;
    }

    /**
     * 状态值 1正常 2已被购买 3删除
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'              => $this->_id,
            'sparring_buy_id' => $this->_sparringBuyId,
            'game_id'         => $this->_gameId,
            'net_bar'         => $this->_netBar,
            'price'           => $this->_price,
            'begin_time'      => $this->_beginTime,
            'end_time'        => $this->_endTime,
            'game_level'      => $this->_gameLevel,
            'game_server'     => $this->_gameServer,
            'manifesto'       => $this->_manifesto,
            'gender'          => $this->_gender,
            'add_time'        => $this->_addTime,
            'update_time'     => $this->_updateTime,
            'status'          => $this->_status
        );
    }

}
