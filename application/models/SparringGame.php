<?php

/**
 * Sparring_game
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: sparring_game
 */
class SparringGameModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * 指定游戏
     * 
     * Column Type: varchar(30)
     * 
     * @var string
     */
    protected $_gameName = '';

    /**
     * 状态 1-正常 0-禁用
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_status = 0;

    /**
     * 排序
     * 
     * Column Type: tinyint(3) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_order = 0;

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_updateTime = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \SparringGameModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * 指定游戏
     * 
     * Column Type: varchar(30)
     * 
     * @param string $gameName
     * @return \SparringGameModel
     */
    public function setGame_name($gameName) {
        $this->_gameName = (string)$gameName;

        return $this;
    }

    /**
     * 指定游戏
     * 
     * Column Type: varchar(30)
     * 
     * @return string
     */
    public function getGame_name() {
        return $this->_gameName;
    }

    /**
     * 状态 1-正常 0-禁用
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $status
     * @return \SparringGameModel
     */
    public function setStatus($status) {
        $this->_status = (int)$status;

        return $this;
    }

    /**
     * 状态 1-正常 0-禁用
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * 排序
     * 
     * Column Type: tinyint(3) unsigned
     * Default: 0
     * 
     * @param int $order
     * @return \SparringGameModel
     */
    public function setOrder($order) {
        $this->_order = (int)$order;

        return $this;
    }

    /**
     * 排序
     * 
     * Column Type: tinyint(3) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getOrder() {
        return $this->_order;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addTime
     * @return \SparringGameModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $updateTime
     * @return \SparringGameModel
     */
    public function setUpdate_time($updateTime) {
        $this->_updateTime = (int)$updateTime;

        return $this;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getUpdate_time() {
        return $this->_updateTime;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'          => $this->_id,
            'game_name'   => $this->_gameName,
            'status'      => $this->_status,
            'order'       => $this->_order,
            'add_time'    => $this->_addTime,
            'update_time' => $this->_updateTime
        );
    }

}
