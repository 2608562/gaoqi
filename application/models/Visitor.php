<?php

/**
 * Visitor
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: visitor
 */
class VisitorModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * 用户id
     * 
     * Column Type: int(10) unsigned
     * 
     * @var int
     */
    protected $_uid = null;

    /**
     * 访问者的id
     * 
     * Column Type: int(10) unsigned
     * 
     * @var int
     */
    protected $_visitorId = null;

    /**
     * 访问时间
     * 
     * Column Type: int(10) unsigned
     * 
     * @var int
     */
    protected $_updateTime = null;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \VisitorModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * 用户id
     * 
     * Column Type: int(10) unsigned
     * 
     * @param int $uid
     * @return \VisitorModel
     */
    public function setUid($uid) {
        $this->_uid = (int)$uid;

        return $this;
    }

    /**
     * 用户id
     * 
     * Column Type: int(10) unsigned
     * 
     * @return int
     */
    public function getUid() {
        return $this->_uid;
    }

    /**
     * 访问者的id
     * 
     * Column Type: int(10) unsigned
     * 
     * @param int $visitorId
     * @return \VisitorModel
     */
    public function setVisitor_id($visitorId) {
        $this->_visitorId = (int)$visitorId;

        return $this;
    }

    /**
     * 访问者的id
     * 
     * Column Type: int(10) unsigned
     * 
     * @return int
     */
    public function getVisitor_id() {
        return $this->_visitorId;
    }

    /**
     * 访问时间
     * 
     * Column Type: int(10) unsigned
     * 
     * @param int $updateTime
     * @return \VisitorModel
     */
    public function setUpdate_time($updateTime) {
        $this->_updateTime = (int)$updateTime;

        return $this;
    }

    /**
     * 访问时间
     * 
     * Column Type: int(10) unsigned
     * 
     * @return int
     */
    public function getUpdate_time() {
        return $this->_updateTime;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'          => $this->_id,
            'uid'         => $this->_uid,
            'visitor_id'  => $this->_visitorId,
            'update_time' => $this->_updateTime
        );
    }

}
