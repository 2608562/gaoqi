<?php

/**
 * Withdraw
 * 
 * @Table Schema: gaoqi_api
 * @Table Name: withdraw
 */
class WithdrawModel extends \Base\Model\AbstractModel {

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * Uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_uid = 0;

    /**
     * Sum
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @var float
     */
    protected $_sum = 0.00;

    /**
     * Real_name
     * 
     * Column Type: varchar(45)
     * 
     * @var string
     */
    protected $_realName = null;

    /**
     * Account
     * 
     * Column Type: varchar(50)
     * 
     * @var string
     */
    protected $_account = '';

    /**
     * Type
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_type = 0;

    /**
     * Remark
     * 
     * Column Type: varchar(255)
     * 
     * @var string
     */
    protected $_remark = null;

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_addTime = 0;

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_updateTime = 0;

    /**
     * 1-待审核 2-审核同意 3-审核拒绝
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @var int
     */
    protected $_status = 0;

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @param int $id
     * @return \WithdrawModel
     */
    public function setId($id) {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Id
     * 
     * Column Type: int(10) unsigned
     * auto_increment
     * PRI
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * Uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $uid
     * @return \WithdrawModel
     */
    public function setUid($uid) {
        $this->_uid = (int)$uid;

        return $this;
    }

    /**
     * Uid
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getUid() {
        return $this->_uid;
    }

    /**
     * Sum
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @param float $sum
     * @return \WithdrawModel
     */
    public function setSum($sum) {
        $this->_sum = (float)$sum;

        return $this;
    }

    /**
     * Sum
     * 
     * Column Type: decimal(10,2) unsigned
     * Default: 0.00
     * 
     * @return float
     */
    public function getSum() {
        return $this->_sum;
    }

    /**
     * Real_name
     * 
     * Column Type: varchar(45)
     * 
     * @param string $realName
     * @return \WithdrawModel
     */
    public function setReal_name($realName) {
        $this->_realName = (string)$realName;

        return $this;
    }

    /**
     * Real_name
     * 
     * Column Type: varchar(45)
     * 
     * @return string
     */
    public function getReal_name() {
        return $this->_realName;
    }

    /**
     * Account
     * 
     * Column Type: varchar(50)
     * 
     * @param string $account
     * @return \WithdrawModel
     */
    public function setAccount($account) {
        $this->_account = (string)$account;

        return $this;
    }

    /**
     * Account
     * 
     * Column Type: varchar(50)
     * 
     * @return string
     */
    public function getAccount() {
        return $this->_account;
    }

    /**
     * Type
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $type
     * @return \WithdrawModel
     */
    public function setType($type) {
        $this->_type = (int)$type;

        return $this;
    }

    /**
     * Type
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getType() {
        return $this->_type;
    }

    /**
     * Remark
     * 
     * Column Type: varchar(255)
     * 
     * @param string $remark
     * @return \WithdrawModel
     */
    public function setRemark($remark) {
        $this->_remark = (string)$remark;

        return $this;
    }

    /**
     * Remark
     * 
     * Column Type: varchar(255)
     * 
     * @return string
     */
    public function getRemark() {
        return $this->_remark;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $addTime
     * @return \WithdrawModel
     */
    public function setAdd_time($addTime) {
        $this->_addTime = (int)$addTime;

        return $this;
    }

    /**
     * Add_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getAdd_time() {
        return $this->_addTime;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @param int $updateTime
     * @return \WithdrawModel
     */
    public function setUpdate_time($updateTime) {
        $this->_updateTime = (int)$updateTime;

        return $this;
    }

    /**
     * Update_time
     * 
     * Column Type: int(10) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getUpdate_time() {
        return $this->_updateTime;
    }

    /**
     * 1-待审核 2-审核同意 3-审核拒绝
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @param int $status
     * @return \WithdrawModel
     */
    public function setStatus($status) {
        $this->_status = (int)$status;

        return $this;
    }

    /**
     * 1-待审核 2-审核同意 3-审核拒绝
     * 
     * Column Type: tinyint(1) unsigned
     * Default: 0
     * 
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }

    /**
     * Return a array of model properties
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id'          => $this->_id,
            'uid'         => $this->_uid,
            'sum'         => $this->_sum,
            'real_name'   => $this->_realName,
            'account'     => $this->_account,
            'type'        => $this->_type,
            'remark'      => $this->_remark,
            'add_time'    => $this->_addTime,
            'update_time' => $this->_updateTime,
            'status'      => $this->_status
        );
    }

}
