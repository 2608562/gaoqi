<?php

use Mapper\CommentModel as CommentMapper;
use Mapper\MemberModel as MemberMapper;
use Mapper\FriendcircleModel as FriendCircleMapper;

class CommentController extends \Base\ApiController {

    /**
     * @desc 添加评论
     *
     * @return false
     */
    public function addAction() {
        $request = $this->getRequest();
        $fromUid = \Lib\User::getInstance()->id();
        $toUid   = (int)$request->get('to_uid');
        $cid     = (int)$request->get('cid');
        $content = (string)$request->get('content');
        $type    = (int)$request->get('type');

        if(empty($content)) {
            return \Response::error('内容不能为空', 10313);
        }

        $commentMapper = CommentMapper::getInstance();

        $model = new \CommentModel();
        $model->setCid($cid);
        $model->setFrom_uid($fromUid);
        $model->setTo_uid($toUid);
        $model->setContent($content);
        $model->setType($type);
        $model->setStatus(\Lib\Consts::COMMENT_STATUS_NORMAL);
        $model->setAdd_time(time());

        $id = $commentMapper->insert($model);

        if($id > 0) {
            if(\Lib\Consts::COMMENT_TYPE_FRIEND_CIRCLE === $type) {
                $circleMapper = FriendCircleMapper::getInstance();
                $circleModel  = $circleMapper->findById($cid);

                if(!$circleModel instanceof \FriendCircleModel) {
                    return \Response::error('无效的朋友圈id', 10310);
                }

                $memberMapper = MemberMapper::getInstance();
                $memberModel  = $memberMapper->findById($circleModel->getMember_id());

                if(!$memberModel instanceof \MemberModel) {
                    return \Response::error('无效的uid', 10311);
                }

                $imgUrl = $circleModel->getImg_url();

                if(!empty($imgUrl) || strpos($imgUrl, ',') !== false) {
                    $arr    = explode(',', $imgUrl);
                    $imgUrl = $arr[0];
                }

                $msgData = [
                    'info_id'  => $cid,
                    'avatar'   => $memberModel->getAvatar(),
                    'nickname' => urldecode($memberModel->getNick_name()),
                    'content'  => $content,
                    'img_url'  => $imgUrl, // 此条朋友圈的第一张图片
                    'time'     => date('m月d日 H:s'),
                ];
                \Lib\MsgPush::msgadd(\Lib\Consts::MESSAGE_PUSH_FRIEND_CIRCLE, $msgData);
            }

            return \Response::data(null);
        }

        return \Response::error('评论添加失败', 10306);
    }

    /**
     * @desc 删除评论
     *
     * @return false
     */
    public function delAction() {
        $request = $this->getRequest();
        $id      = (int)$request->get('id');
        $type    = (int)$request->get('type');

        $commentMapper = CommentMapper::getInstance();
        $commentModel  = $commentMapper->fetch([
            'type'   => $type,
            'id'     => $id,
            'status' => \Lib\Consts::COMMENT_STATUS_NORMAL,
        ]);

        if(!$commentModel instanceof \CommentModel) {
            return \Response::error('评论不存在', 10308);
        }

        $data = [
            'status' => \Lib\Consts::COMMENT_STATUS_DEL,
        ];

        try {
            $rows = $commentMapper->update($data, [
                'type'   => $type,
                'id'     => $id,
                'status' => \Lib\Consts::COMMENT_STATUS_NORMAL,
            ]);
            return $rows > 0 ? \Response::data(null) : \Response::error('评论删除失败', 10307);
        } catch(\Exception $exc) {
            return \Response::error('异常信息报错', 10100);
        }
    }

    /**
     * @desc 评论列表
     */
    public function listAction() {
        $request = $this->getRequest();
        $cid     = (int)$request->get('cid');
        $uid     = \Lib\User::getInstance()->id();
        $type    = (int)$request->get('type');

        $commentMapper = CommentMapper::getInstance();
        $commentModels = $commentMapper->fetchAll([
            'type'   => \Lib\Consts::COMMENT_TYPE_FRIEND_CIRCLE,
            'status' => \Lib\Consts::COMMENT_STATUS_NORMAL,
            'cid'    => $cid,
        ], 'add_time');

        $memberMapper = MemberMapper::getInstance();

        $return = [];
        $isDel  = '0';

        if(\Lib\Consts::COMMENT_TYPE_FRIEND_CIRCLE === $type) {
            $friendCircleMapper = FriendCircleMapper::getInstance();
            $friendCircleModel  = $friendCircleMapper->findById($cid);

            if(!$friendCircleModel instanceof \FriendCircleModel) {
                return \Response::error('无效的朋友圈id', 10302);
            }

            $circleUid = $friendCircleModel->getMember_id();

            if($circleUid === $uid) {
                $isDel = '1';
            }
        }

        foreach($commentModels as $commentModel) {
            $fromUid         = $commentModel->getFrom_uid();
            $fromMemberModel = $memberMapper->findById($fromUid);

            if(!$fromMemberModel instanceof \MemberModel) {
                return \Response::error('无效的uid', 10311);
            }

            $toMemberModel = $memberMapper->findById($commentModel->getTo_uid());

            if(!$toMemberModel instanceof \MemberModel) {
                return \Response::error('无效的uid', 10311);
            }

            $data              = $commentModel->toArray();
            $data['avatar']    = $fromMemberModel->getAvatar();
            $data['from_name'] = urldecode($fromMemberModel->getNick_name());
            $data['to_name']   = urldecode($toMemberModel->getNick_name());
            $data['is_del']    = '0';
            $time              = $commentModel->getAdd_time();
            $data['time']      = !empty($time) ? date('m月d日 H:i', $time) : '';

            if($isDel === '1' || $uid === $fromUid) {
                $data['is_del'] = '1'; // 可删除
            }

            $return[] = $data;
        }

        return \Response::data($return);
    }
}
