<?php

use Mapper\ConfigFeedbackModel as ConfigFeedbackMapper;
use Mapper\MemberModel as MemberMapper;
use Mapper\GamecirclememberModel as GameCircleMemberMapper;

class ConfigController extends \Base\ApiController {
    /**
     * @des 意见反馈
     *
     * @return false
     */
    public function feedbackAction() {
        $uid     = \Lib\User::getInstance()->id();
        $request = $this->getRequest();
        $type    = (int)$request->get('type');
        $content = (string)$request->get('content');

        if($type < 0) {
            return Response::error('反馈类型不能为空', 202010);
        }

        if(empty($content)) {
            return Response::error('反馈内容不能为空', 202011);
        }

        $model = new \ConfigFeedbackModel();
        $model->setMember_id($uid);
        $model->setType($type);
        $model->setContent($content);
        $model->setAdd_time(time());
        $model->setStatus(Lib\Consts::CONFIG_FEEDBACK_NOT_DONE); //消息的状态，默认为 0-未处理

        $configFeedbackMapper = ConfigFeedbackMapper::getInstance();

        try {
            $id = $configFeedbackMapper->insert($model);
            return $id ? Response::data($id, '反馈信息添加成功') : Response::error('反馈信息添加失败', 202012);

        } catch(Exception $exc) {
            $err = $exc->getMessage();
            return Response::error($err, 20200);
        }

    }

    /**
     * @desc 通知设置
     * @param string $type 类型
     * @param int $status 状态
     *
     * @return false
     */
    public function informAction() {
        $uid                = Lib\User::getInstance()->id();
        $request            = $this->getRequest();
        $type               = (string)$request->get('type');
        $status             = (int)$request->get('status');
        $memberSwitchMapper = \Mapper\MemberSwitchModel::getInstance();

        if( empty($type) ) {
            return \Response::error('设置的类型不能为空', 20210);
        }

        if(empty($status)) {
            return \Response::error('设置的内容不能为空', 20211);
        }

        if($type === 'receiver') {
            $where = ['receiver_inform' => $status];
        } elseif($type === 'hide') {
            $where = ['inform_type' => $status];
        }

        try {
            $memberSwitchMapper->update($where, ['uid' => $uid]);
            return Response::data('消息通知设置成功');
        } catch(Exception $exc) {
            return Response::error('异常信息报错', 10100);
        }

    }

    /**
     * @des 电竞圈消息设置
     *
     * @return false
     */
    public function gameCircleInformAction() {
        $uid          = Lib\User::getInstance()->id();
        $request      = $this->getRequest();
        $gameCircleId = (int)$request->get('game_circle_id');
        $messageSet   = (int)$request->get('message_set');

        if($gameCircleId <= 0) {
            return Response::error('电竞圈的id不能为空', 20212);
        }

        if($messageSet <= 0) {
            return Response::error('电竞圈的消息通知类型不能为空', 20213);
        }

        $gameCircleMemberMapper = GameCircleMemberMapper::getInstance();

        try {
            $rows = $gameCircleMemberMapper->update(['msg_setting' => $messageSet], ['member_id' => $uid, 'game_circle_id' => $gameCircleId]);
            return $rows ? Response::data($rows, '电竞圈消息通知设置成功') : Response::error('电竞圈消息通知设置失败', 20214);
        } catch(Exception $exc) {
            return Response::error('异常信息报错', 10100);
        }

    }

    /**
     * @desc 获取协议
     *
     * @return false
    */

    public function protocolAction() {
        $type = strtolower($this->getRequest()->get('type'));
        $allowType = [
            'gaoqi'    => 1,
            'member'   => 1,
            'group'    => 1,
            'sparring' => 1,
            'secret'   => 1,
        ];

        if(isset($allowType[$type])) {
            $url = 'http://'.$_SERVER['HTTP_HOST'].'/protocol/show?type='.$type;
            return \Response::data($url);
        }

    }

    /**
     * @des 系统评分
     *
     * @return false
     */
    public function assessAction() {
        //安卓市场 和appstore
        $uid     = Lib\User::getInstance()->id();
        $request = (int)$this->get();

    }

}
