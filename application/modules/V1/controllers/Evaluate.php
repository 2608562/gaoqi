<?php

use Mapper\OrderModel as OrderMapper;
use Mapper\EvaluateModel as EvaluateMapper;

class EvaluateController extends \Base\ApiController {

    /**
     * @desc 评价列表
     *
     * @return false
     */
    public function listAction() {
        $uid      = \Lib\User::getInstance()->id();
        $request  = $this->getRequest();
        $page     = (int)$request->get('page', 1);
        $perPage  = (int)$request->get('per_page', 15);
        $page     = max(1, $page);
        $perPage  = min(30, $perPage);
        $limit    = $perPage;
        $offset   = $limit * ($page - 1);
        $orderIds = [];
        $return   = [];

        $orderMapper = OrderMapper::getInstance();
        $orderModels = $orderMapper->fetchAll([
            'from_uid' => $uid,
            'status'   => \Lib\Consts::ORDER_STATUS_EVALUATE,
        ], 'id', $limit, $offset);

        if(empty($orderModels)) {
            return \Response::data([]);
        }

        foreach($orderModels as $orderModel) {
            $orderIds[] = $orderModel->getId();
        }

        $evaluateMapper = EvaluateMapper::getInstance();
        $evaluateModels = $evaluateMapper->fetchAll(['order_id' => $orderIds], 'add_time DESC');

        foreach($evaluateModels as $evaluateModel) {
            $data         = $evaluateModel->toArray();
            $data['time'] = date('Y-m-d', $evaluateModel->getAdd_time());
            $return[]     = $data;
        }

        return \Response::data($return);
    }

    public function addAction() {
        $request = $this->getRequest();
        $starNum = (int)$request->get('star_num');
        $content = (string)trim($request->get('content'));
        $orderId = (int)$request->get('order_id');

        if($orderId <= 0) {
            return \Response::error('订单id有误', 10702);
        }

        if($starNum <= 0 || $starNum > 5) {
            return \Response::error('星星数有误', 10701);
        }

        if(empty($content)) {
            return \Response::error('内容不能为空', 10704);
        }

        $orderMapper = OrderMapper::getInstance();
        $orderModel  = $orderMapper->findById($orderId);

        if(!$orderModel instanceof \OrderModel) {
            return \Response::error('无效的订单id', 10703);
        }

        if($orderModel->getStatus() !== \Lib\Consts::ORDER_STATUS_FINISH) {
            return \Response::error('订单状态值有误', 10609);
        }

        $evaluateMapper = EvaluateMapper::getInstance();
        $evaluateMapper->begin();

        $model = new \EvaluateModel();
        $model->setOrder_id($orderId);
        $model->setStar_num($starNum);
        $model->setContent($content);
        $model->setAdd_time(time());

        try {
            $eId = $evaluateMapper->insert($model);
        } catch(\Exception $exc) {
            $eId = 0;
        }

        if($eId <= 0) {
            $evaluateMapper->rollback();
            return \Response::error('评价失败', 10607);
        }

        $data = [
            'status'      => \Lib\Consts::ORDER_STATUS_EVALUATE,
            'update_time' => time(),
        ];

        try {
            $rows = $orderMapper->update($data, ['id' => $orderId, 'status' => \Lib\Consts::ORDER_STATUS_FINISH]);
        } catch(\Exception $e) {
            $rows = 0;
        }

        if($rows <= 0) {
            $evaluateMapper->rollback();
            return \Response::error('订单状态变更失败', 10611);
        }

        $evaluateMapper->commit();
        return \Response::data(null);
    }
}