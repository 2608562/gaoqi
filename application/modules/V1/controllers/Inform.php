<?php

use Mapper\InformModel as InformMapper;
use Mapper\MemberModel as MemberMapper;
use Mapper\FriendcircleModel as FriendCircleMapper;

class InformController extends \Base\ApiController {

    /**
     * @desc 添加举报信息
     *
     * @return false
     */
    public function addAction() {
        $request = $this->getRequest();
        $mobile  = (string)$request->get('mobile');
        $uid     = \Lib\User::getInstance()->id();
        $cid     = (int)$request->get('cid');
        $content = (string)$request->get('content');
        $type    = (int)$request->get('type');

        if(!\Lib\Is::mobileNum($mobile)) {
            return \Response::error('无效的手机号', 10501);
        }

        if(empty($content)) {
            return \Response::error('内容不能为空', 10505);
        }

        $memberMapper = MemberMapper::getInstance();
        $memberModel  = $memberMapper->findById($uid);

        if(!$memberModel instanceof \MemberModel) {
            return \Response::error('无效的UID', 10502);
        }

        if($type === \Lib\Consts::INFORM_TYPE_GAME_CIRCLE) {
            $circleMapper = FriendCircleMapper::getInstance();
            $circleModel  = $circleMapper->findById($cid);

            if(!$circleModel instanceof \FriendCircleModel) {
                return \Response::error('无效的电竞圈id', 10503);
            }
        }

        $informMapper = InformMapper::getInstance();

        $model = new \InformModel();
        $model->setCid($cid);
        $model->setUid($uid);
        $model->setMobile($mobile);
        $model->setContent($content);
        $model->setType($type);
        $model->setStatus(\Lib\Consts::INFORM_STATUA_NORMAL);
        $model->setAdd_time(time());

        try {
            $id = $informMapper->insert($model);
            return $id > 0 ? \Response::data(null) : \Response::error('举报失败', 10504);
        } catch(\Exception $exc) {
            return \Response::error('异常信息报错', 10100);
        }
    }


}
