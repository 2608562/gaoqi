<?php

class JpushController extends \Base\ApiController {

    public function pushAction() {
        $request = $this->getRequest();
        $receive = (string)$request->get('receive');
        $receive = ['alias' =>[$receive]];
        $content = (string)$request->get('content');
        $type  = (string)$request->get('type');
        $txt   = (string)$request->get('txt');
        $time  = (int)$request->get('time', 86400);

        $return = \Lib\Jpush::getInstance()->send_pub($receive, $content, $type, $txt, $time);

        if ($return === true) {
            return \Response::data(null);
        } else {
            return \Response::error($return);
        }
    }

}
