<?php

use Mapper\VisitorModel as VisitorMapper;

class VisitorController extends \Base\ApiController {
    /**
     * @des 谁看过我的列表
     *
     * @return false
     */
    public function listAction() {
        $uid     = \Lib\User::getInstance()->id();
        $request = $this->getRequest();
        $page    = (int)$request->get('page', 1);
        $prePage = (int)$request->get('pre_page', 15);
        $page    = max(1, $page);
        $prePage = min(30, $prePage);
        $limit   = $prePage;
        $offset  = $limit * ($page - 1);

        $visitorMapper = VisitorMapper::getInstance();
        $memberMapper  = \Mapper\MemberModel::getInstance();
        $models        = $visitorMapper->fetchAll(['uid' => $uid], 'update_time DESC', $limit, $offset);
        $return        = [];

        foreach($models as $model){
            $memberModel = $memberMapper->findByid($model->getVisitor_id());
            $memberData  = $memberModel->toArray();
            $birthday    = $memberModel->getBirthday();
            $age         = date('Y') - date('Y', $birthday) - 1;

            if(date('m', time()) == date('m', $birthday)) {
                if(date('d', time()) > date('d', $birthday)) {
                    ++$age;
                }
            } elseif(date('m', time()) > date('m', $birthday)) {
                ++$age;
            }

            $memberData['age']       = $age;
            $memberData['nick_name'] = urldecode($memberModel->getNick_name());
            $date                    = date('Y-m-d',$model->getUpdate_time());
            $tmp['data'][]           = $memberData;
            $tmp['date']             = $date;

            if(isset($return[$date])) {
                $return[$date]['data'][] = $memberData;
            } else {
                $return[$date] = $tmp;
            }

            unset($tmp);
        }

        $return = array_values($return);

        return Response::data($return);
    }

    /**
     * @des 添加 谁看过我
     *
     * @return false
     */
    public function addAction() {
        $uid       = \Lib\User::getInstance()->id();
        $request   = $this->getRequest();
        $visitorId = (int)$request->get('visitor_id');

        if($visitorId <= 0) {
            return Response::error('被访问者的id不能为空', 20206);
        }

        $visitorMapper = VisitorMapper::getInstance();
        $visitorExist  = $visitorMapper->fetch(['uid' => $visitorId, 'visitor_id' => $uid]);

        //有访问过的情况下，更新访问时间
        if(!empty($visitorExist)) {
            try {
                $rows = $visitorMapper->update(['update_time' => time()], ['uid' => $visitorId, 'visitor_id' => $uid]);
                return $rows ? Response::data(null) : Response::data('访问数据更新失败', 20207);
            } catch(\Exception $exc) {
                return Response::error('异常信息报错', 10100);
            }
        }

        $model = new \VisitorModel();
        $model->setUid($visitorId);
        $model->setVisitor_id($uid);
        $model->setUpdate_time(time());

        try {
            $rows = $visitorMapper->insert($model);
            return $rows ? Response::data(null) : Response::error('访问数据插入失败', 20208);
        } catch(\Exception $exc) {
            return Response::error('异常信息报错', 10100);
        }
    }
}
