<?php

use \Mapper\FinanceLogModel as LogMapper;
use \Mapper\MemberAccountModel as AccountMapper;
use \Mapper\WithdrawModel as WithdrawMapper;

class FinanceController extends \Base\ApiController {

    public function listAction() {
        $request      = $this->getRequest();
        $uid          = $this->uid();
        $page         = (int)$request->get('page', 1);
        $perPage      = (int)$request->get('per_page', 10);
        $fundFlow     = (int)$request->get('fund_flow');
        $page         = max(1, $page);
        $perPage      = min(30, $perPage);
        $limit        = $perPage;
        $offset       = $limit * ($page - 1);
        $return       = [];
        $where['uid'] = $uid;

        if($fundFlow === 1 || $fundFlow === 2) {
            $where['fund_flow'] = $fundFlow;
        }

        $logMapper = LogMapper::getInstance();
        $models    = $logMapper->fetchAll($where, 'id DESC', $limit, $offset);

        if(empty($models)) {
            return $this->response(['data' => []]);
        }

        foreach($models as $model) {
            $data             = $model->toArray();
            $data['time']     = date('m月d日 H:i', $model->getAdd_time());
            $return['data'][] = $data;
        }

        $accountMapper = AccountMapper::getInstance();
        $accountModel  = $accountMapper->findByUid($uid);

        if(!$accountModel instanceof \MemberAccountModel) {
            $return['now_sum'] = 0;
        } else {
            $return['now_sum'] = $accountModel->getNow_sum();
        }

        return $this->response($return);
    }

    /**
     * @desc 提现申请
     */
    public function withdrawAction() {
        $request  = $this->getRequest();
        $uid      = $this->uid();
        $sum      = (int)$request->get('sum'); // 1的倍数
        $account  = (string)trim($request->get('account'));
        $realName = (string)trim($request->get('real_name'));
        $type     = (int)$request->get('type', 1); // 支付宝

        $accountMapper = AccountMapper::getInstance();
        $accountModel  = $accountMapper->findByUid($uid);

        if(!$accountModel instanceof \MemberAccountModel) {
            return $this->response(null, false, 50000, '无效的账户');
        }

        if ($accountModel->getWithdraw_sum() !==0) {
            return $this->response(null, false, 50000, '提现审核后才能再次提现');
        }

        if($sum < 1) {
            return $this->response(null, false, 50000, '提现金额有误');
        }

        if(empty($account)) {
            return $this->response(null, false, 50000, '账户不能为空');
        }

        if($accountModel->getNow_sum() < $sum) {
            return $this->response(null, false, 50000, '提现金额不能大于余额');
        }

        $mapper = WithdrawMapper::getInstance();
        $model  = new \WithdrawModel();
        $model->setSum($sum);
        $model->setUid($uid);
        $model->setReal_name($realName);
        $model->setAccount($account);
        $model->setType($type);
        $model->setAdd_time(time());
        $model->setStatus(\Lib\Consts::W_TYPE_APPLYING);

        $return = \Lib\Finance::getInstance()->flow($uid, \Lib\Consts::F_WITHDRAWING,
            $sum, function () use ($model, $mapper) {
                try {
                    $id = $mapper->insert($model);
                } catch(\Exception $e) {
                    $id = 0;
                }

                return (bool)$id;
            });

        return $return ? $this->response(null) : $this->response(null, false, 50000, '提现失败');
    }

    /**
     * @desc 充值-预留
     */
    public function chargeAction() {
        $return = \Lib\Finance::getInstance()->flow(65535, 4, 30);
        return $this->response($return);
    }

}
