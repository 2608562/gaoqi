<?php


class ImageController extends \Base\ApiController {

    public function tokenAction() {
        $token = \Lib\Qiniu::getInstance()->uploadToken();
        return $this->response(['uptoken' => $token]);
    }

}
