<?php

use \Mapper\MemberModel as MemberMapper;
use \Mapper\OrderModel as OrderMapper;

class PayController extends \Base\ApiController {

    public function wxpayAction() {
//        初始化日志
//        $logHandler = new \Lib\Pay\CLogFileHandler("../logs/" . date('Y-m-d') . '.log');
//        $log        = \Lib\Pay\Log::Init($logHandler, 15);
        $request = $this->getRequest();
        $orderId = (int)$request->get('order_id');

        $uid          = $this->uid();
        $memberMapper = MemberMapper::getInstance();
        $memberModel  = $memberMapper->findById($uid);

        if(!$memberModel instanceof \MemberModel) {
            return $this->response(null, false, 50000, '无效的uid');
        }

        $orderMapper = OrderMapper::getInstance();
        $orderModel  = $orderMapper->findById($orderId);

        if(!$orderModel instanceof \OrderModel) {
            return $this->response(null, false, 50000, '无效的订单id');
        }

        $status = $orderModel->getStatus();

        if($status !== \Lib\Consts::ORDER_STATUS_NOPAY) {
            return $this->response(null, false, 50000, '无效的订单id');
        }

        $openId = $memberModel->getOpen_id();

        $input = new \Lib\Pay\WxPayUnifiedOrder();
        $input->SetBody("搞起app看大腿");
        $input->SetAttach("test");
        $input->SetOut_trade_no($orderModel->getOrder_no());
//        $input->SetTotal_fee((int)($orderModel->getPay_price() * 100));
        $input->SetTotal_fee(1);
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 600));
        $input->SetGoods_tag("test");
        $input->SetNotify_url("http://api.dev.gaoqiapp.com/wx/pay/wxnotify");
        $input->SetTrade_type("JSAPI");
        $input->SetOpenid($openId);

        $order = Lib\Pay\WxPayApi::unifiedOrder($input);

        if(array_key_exists('err_code_des', $order) && !empty($order['err_code_des'])) {
            return $this->response(null, false, 50000, $order['err_code_des']);
        }

        $tools           = new Lib\Pay\JsApiPay;
        $jsApiParameters = $tools->GetJsApiParameters($order);

        return $this->response($jsApiParameters);
    }

    public function wxnotifyAction() {
        $notify = new \Lib\Pay\PayNotifyCallBack();
        $notify->Handle(false);
        return false;
    }

    public function testAction() {
        $data = [
//            'order_no'    => urldecode('%F0%9F%8C%B0'),
            'order_no'    => '\ud83c\udf30',
            'time'        => '12:00',
            'cancel_time' => '2000年00月00日',
            'price'       => 10,
        ];

        $return = \Lib\Queue::push('wxmsg', [
            'touser'       => "oA1sXuDNWQwiKVqBUMj6kUEXzp7k",
            'template_id'  => \Lib\Consts::TEMP_ID_REFUND,
            'url'          => 'wx.dev.gaoqiapp.com/order_detail.html?order_id=120&fund_flow=1',
            'data'         => \Lib\Wechat::getInstance()->getTmp(\Lib\Consts::TEMP_ID_REFUND, $data),
        ]);

        return $this->response($return);
    }

}