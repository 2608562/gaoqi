SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `member`
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号',
  `password` char(43) NOT NULL DEFAULT '' COMMENT '用户密码',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '用户状态 1-正常 2禁用',
  `regist_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `nick_name` varchar(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `letter` char(1) NOT NULL COMMENT '昵称拼音的首字母',
  `gender` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '性别',
  `main_game` varchar(30) DEFAULT '' COMMENT '主打游戏',
  `character_sign` varchar(100) DEFAULT '' COMMENT '个性签名',
  `relation_status` varchar(30) DEFAULT '' COMMENT '情感状态',
  `hobby` varchar(100) DEFAULT '' COMMENT '兴趣爱好',
  `birthday` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '生日',
  `profession` varchar(30) DEFAULT '' COMMENT '职业',
  `birthplace` varchar(30) DEFAULT '' COMMENT '出生地',
  `appearance` varchar(255) DEFAULT '' COMMENT '外貌',
  `love_times` varchar(30) DEFAULT '' COMMENT '恋爱经历',
  `month_income` varchar(30) DEFAULT '' COMMENT '月收入',
  `housing` varchar(30) DEFAULT '' COMMENT '住房情况',
  `school` varchar(50) DEFAULT '' COMMENT '毕业学校',
  `company` varchar(100) DEFAULT '' COMMENT '就职公司',
  `game_zone` varchar(50) DEFAULT '' COMMENT '游戏区',
  `game_account` varchar(30) DEFAULT '' COMMENT '游戏id',
  `good_role` varchar(30) DEFAULT '' COMMENT '擅长角色',
  `quotations` varchar(50) DEFAULT '' COMMENT '搞起语录',
  `else_game` varchar(50) DEFAULT '' COMMENT '其他游戏',
  `longitude` int(10) unsigned DEFAULT '0' COMMENT '经度(最大180)',
  `latitude` int(10) unsigned DEFAULT '0' COMMENT '纬度(最大90)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
