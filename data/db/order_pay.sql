use gaoqi_api;
DROP TABLE IF EXISTS `order_pay`;

CREATE TABLE `order_pay` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL DEFAULT '0',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pay_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `transaction_id` varchar(28) CHARACTER SET utf8mb4 NOT NULL,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4