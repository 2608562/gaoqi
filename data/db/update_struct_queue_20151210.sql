CREATE TABLE `queue` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('message','sms','email') NOT NULL DEFAULT 'message',
  `content` varchar(2048) NOT NULL COMMENT 'JSON格式内容数据',
  `status` enum('pending','processing','processed','failed','successed') NOT NULL DEFAULT 'pending',
  `time` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='队列 ';
